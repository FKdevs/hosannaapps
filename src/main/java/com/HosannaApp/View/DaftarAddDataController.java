package com.HosannaApp.View;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.Staff;
import com.HosannaApp.Util.AlertHelper;
import com.HosannaApp.Util.AutoCompleteComboBoxListener;
import com.HosannaApp.Util.DateUtil;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DaftarAddDataController {
	private MainApp mainApp;
	private Stage dialogStage;
	@FXML
	private ComboBox<String> customerField;
	@FXML
	private ComboBox<String> jenisPermintaanField;
	@FXML
	private ComboBox<String> staffField;
	@FXML
	private DatePicker tanggalOrderField;
	@FXML
	private TextField nomorDOField;
	@FXML
	private TextField namaMerekField;
	@FXML
	private TextField kelasField;
	@FXML
	private TextField keteranganOrderField;
	@FXML
	private TableView<Daftar> daftarAddTable;
	@FXML
	private TableColumn<Daftar, String> nomorColumn;
	@FXML
	private TableColumn<Daftar, String> customerColumn;
	@FXML
	private TableColumn<Daftar, String> jenisPermintaanColumn;
	@FXML
	private TableColumn<Daftar, String> staffColumn;
	@FXML
	private TableColumn<Daftar, String> namaMerekColumn;
	@FXML
	private TableColumn<Daftar, String> kelasColumn;
	@FXML
	private TableColumn<Daftar, String> keteranganOrderColumn;

	private int daftarTempIndex;
	private Daftar daftarTemp;
	private ObservableList<Daftar> daftarTemporaryList = FXCollections.observableArrayList();
	private ObservableList<String> customerDataInString;
	private ObservableList<String> staffDataInString;
	private ObservableList<String> optionsStatus = FXCollections.observableArrayList("Periksa", "Daftar Baru",
			"Perpanjang");

	@FXML
	private void initialize() {
		tanggalOrderField.setConverter(DateUtil.getDateConverter());
		tanggalOrderField.setValue(LocalDate.now());

		nomorColumn.setCellFactory(cellData -> {
			TableCell<Daftar, String> indexCell = new TableCell<>();
			indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
					.otherwise(indexCell.indexProperty().add(1).asString()));
			return indexCell;
		});
		customerColumn.setCellValueFactory(cellData -> cellData.getValue().getCustomer().getNamaAlamatProperty());
		namaMerekColumn.setCellValueFactory(cellData -> cellData.getValue().getNamaMerek());
		kelasColumn.setCellValueFactory(cellData -> cellData.getValue().getKelas());
		jenisPermintaanColumn.setCellValueFactory(cellData -> cellData.getValue().getJenisPermintaan());
		keteranganOrderColumn.setCellValueFactory(cellData -> cellData.getValue().getKeteranganOrder());
		staffColumn.setCellValueFactory(cellData -> cellData.getValue().getStaff().getNamaProperty());
		
		daftarAddTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		customerDataInString = FXCollections.observableArrayList();
		staffDataInString = FXCollections.observableArrayList();
		daftarAddTable.setItems(daftarTemporaryList);
		daftarAddTable.refresh();
		for (Customer customer : mainApp.getCustomerList()) {
			customerDataInString.add(customer.getNama() + "|" + customer.getAlamat());
		}
		for (Staff staff : mainApp.getStaffList()) {
			staffDataInString.add(staff.getNamaValue());
		}
		staffField.setItems(staffDataInString);
		new AutoCompleteComboBoxListener(staffField);
		customerField.setItems(customerDataInString);
		new AutoCompleteComboBoxListener(customerField);
		jenisPermintaanField.setItems(optionsStatus);
	}
	
	public void setNomorDO(String nomorDO) {
		nomorDOField.setText(nomorDO);
	}
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	@FXML
	private void handleTableSelect() {
		daftarTemp = daftarAddTable.getSelectionModel().getSelectedItem();
		daftarTempIndex = daftarAddTable.getSelectionModel().getSelectedIndex();
		if (daftarTemp != null) {
			customerField.setValue(daftarTemp.getCustomer().getNama() + "|" + daftarTemp.getCustomer().getAlamat());
			namaMerekField.setText(daftarTemp.getNamaMerekValue());
			kelasField.setText(daftarTemp.getKelasValue());
			jenisPermintaanField.setValue(daftarTemp.getJenisPermintaanValue());
			keteranganOrderField.setText(daftarTemp.getKeteranganOrderValue());
			staffField.setValue(daftarTemp.getStaff().getNamaValue());
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleConfirm() {
		for(Daftar daftar : daftarTemporaryList) {
			try {
				DatabaseExecutor.addNewDaftarToDatabase(daftar);
				this.mainApp.addDaftarList(daftar);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		this.dialogStage.close();
	}

	@FXML
	private void handleAddDataTable() {
		if (inputDaftarCheck()) {
			Customer customerTemp = searchCustomer(customerField.getValue().split("\\|")[0]);
			Daftar daftar = new Daftar("", nomorDOField.getText(), customerTemp, searchStaff(staffField.getValue()),
					jenisPermintaanField.getValue(), namaMerekField.getText(), kelasField.getText(),
					keteranganOrderField.getText(), tanggalOrderField.getValue());
			daftarTemporaryList.add(daftar);
			this.clearInputForm();
		}
	}

	@FXML
	private void handleDeleteDataTable() {
		int selectedIndex = daftarAddTable.getSelectionModel().getSelectedIndex();
		Daftar daftar = daftarAddTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Delete Data", "Delete Data",
					"Are you sure to Delete data from index number: " + Integer.toString(daftarTempIndex + 1) + " ?");
			if (result.get() == ButtonType.OK) {
				this.daftarTemporaryList.remove(daftar);
				this.daftarAddTable.refresh();
				clearInputForm();
			} else {
				clearInputForm();
			}
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleClear() {
		clearInputForm();
	}

	private void clearInputForm() {
		customerField.setValue("");
		namaMerekField.clear();
		kelasField.clear();
		jenisPermintaanField.setValue("");
		keteranganOrderField.clear();
		tanggalOrderField.setValue(LocalDate.now());
		staffField.setValue("");
	}

	private boolean inputDaftarCheck() {
		Customer searchCustomerTemp = searchCustomer(customerField.getValue().split("\\|")[0]);
		if (customerField.getSelectionModel().isEmpty() || searchCustomerTemp == null) {
			AlertHelper.showErrorWarning("Data Kurang", "Nama Customer Field", "Nama Customer Field Kosong/Invalid");
			return false;
		}
		if (jenisPermintaanField.getValue().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Jenis Permintaan Field", "Jenis Permintaan Field Kosong");
			return false;
		}
		if (namaMerekField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Merek Field", "Merek Field Kosong");
			return false;
		}
		if (staffField.getValue().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Staff Field", "Staff Field Kosong");
			return false;
		}
		return true;
	}

	private Customer searchCustomer(String namaCustomer) {
		for (Customer customer : mainApp.getCustomerList()) {
			if (customer.getNama().equals(namaCustomer)) {
				return customer;
			}
		}
		return null;
	}

	private Staff searchStaff(String namaStaff) {
		for (Staff staff : mainApp.getStaffList()) {
			if (staff.getNamaValue().equals(namaStaff)) {
				return staff;
			}
		}
		return null;
	}
}