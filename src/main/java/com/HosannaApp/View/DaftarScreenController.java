package com.HosannaApp.View;

import java.time.LocalDate;
import java.util.Optional;

import com.HosannaApp.DatabaseHelper;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Util.AlertHelper;

import javafx.beans.binding.Bindings;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class DaftarScreenController {
	private MainApp mainApp;
	@FXML
	private TableView<Daftar> daftarTable;
	@FXML
	private TableColumn<Daftar, String> nomorDOColumn;
	@FXML
	private TableColumn<Daftar, String> tanggalOrderColumn;
	@FXML
	private TableColumn<Daftar, String> customerColumn;
	@FXML
	private TableColumn<Daftar, String> jenisPermintaanColumn;
	@FXML
	private TableColumn<Daftar, String> namaMerekColumn;
	@FXML
	private TableColumn<Daftar, String> kelasColumn;
	@FXML
	private TableColumn<Daftar, String> keteranganOrderColumn;
	@FXML
	private TableColumn<Daftar, String> staffColumn;
	@FXML
	private TextField nomorDOFieldFilter;
	@FXML
	private TextField tanggalOrderFieldFilter;
	@FXML
	private TextField customerFieldFilter;
	@FXML
	private TextField jenisPermintaanFieldFilter;
	@FXML
	private TextField namaMerekFieldFilter;
	@FXML
	private TextField kelasFieldFilter;
	@FXML
	private TextField keteranganOrderFieldFilter;
	@FXML
	private TextField staffFieldFilter;
	@FXML
	private ContextMenu daftarContextMenu;

	private int daftarTempIndex;
	private Daftar daftarTemp;
	private SortedList<Daftar> sortedData;

	public DaftarScreenController() {
	}

	@FXML
	private void initialize() {
		nomorDOColumn.setCellValueFactory(cellData -> cellData.getValue().getNomorDO());
		tanggalOrderColumn.setCellValueFactory(cellData -> cellData.getValue().getTanggalOrderProperty());
		customerColumn.setCellValueFactory(cellData -> cellData.getValue().getCustomer().getNamaProperty());
		jenisPermintaanColumn.setCellValueFactory(cellData -> cellData.getValue().getJenisPermintaan());
		namaMerekColumn.setCellValueFactory(cellData -> cellData.getValue().getNamaMerek());
		kelasColumn.setCellValueFactory(cellData -> cellData.getValue().getKelas());
		keteranganOrderColumn.setCellValueFactory(cellData -> cellData.getValue().getKeteranganOrder());
		staffColumn.setCellValueFactory(cellData -> cellData.getValue().getStaff().getNamaProperty());
		
		daftarTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void handleTableMenu(MouseEvent e) {
		daftarTemp = daftarTable.getSelectionModel().getSelectedItem();
		daftarTempIndex = daftarTable.getSelectionModel().getSelectedIndex();

		daftarContextMenu = new ContextMenu();
		MenuItem updateKasirMenuItem = new MenuItem("Update Kasir");
		MenuItem updateDataProsesMenuItem = new MenuItem("Update Data Proses");
		updateKasirMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				showEditCashierDataDaftarScreen(daftarTemp);
			}
		});
		updateDataProsesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				showEditCashierDataDaftarScreen(daftarTemp, daftarTable);
			}
		});
		daftarContextMenu.getItems().add(updateKasirMenuItem);
		daftarContextMenu.getItems().add(updateDataProsesMenuItem);
		if (e.getButton() == MouseButton.SECONDARY) {
			daftarTable.setContextMenu(daftarContextMenu);
		}
	}

	public void showEditCashierDataDaftarScreen(Daftar daftar) {
		this.mainApp.showEditCashierDataDaftarScreen(daftar);
	}

	public void showEditCashierDataDaftarScreen(Daftar daftar, TableView<Daftar> daftarTable) {
		this.mainApp.showEditDataProcessDataDaftarScreen(daftar, daftarTable);
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		FilteredList<Daftar> filteredData = new FilteredList<>(mainApp.getDaftarList(), p -> true);
		filteredData.predicateProperty().bind(Bindings.createObjectBinding(() -> daftar -> daftar.getNomorDOValue()
				.toLowerCase().contains(nomorDOFieldFilter.getText().toLowerCase())
				&& daftar.getTanggalOrder().toLowerCase().contains(tanggalOrderFieldFilter.getText().toLowerCase())
				&& daftar.getCustomer().getNama().toLowerCase().contains(customerFieldFilter.getText().toLowerCase())
				&& daftar.getJenisPermintaanValue().toLowerCase()
						.contains(jenisPermintaanFieldFilter.getText().toLowerCase())
				&& daftar.getNamaMerekValue().toLowerCase().contains(namaMerekFieldFilter.getText().toLowerCase())
				&& daftar.getKelasValue().toLowerCase().contains(kelasFieldFilter.getText().toLowerCase())
				&& daftar.getKeteranganOrderValue().toLowerCase()
						.contains(keteranganOrderFieldFilter.getText().toLowerCase())
				&& daftar.getStaff().getNamaValue().toLowerCase().contains(staffFieldFilter.getText().toLowerCase()),

				nomorDOFieldFilter.textProperty(), tanggalOrderFieldFilter.textProperty(),
				customerFieldFilter.textProperty(), jenisPermintaanFieldFilter.textProperty(),
				namaMerekFieldFilter.textProperty(), kelasFieldFilter.textProperty(),
				keteranganOrderFieldFilter.textProperty(), staffFieldFilter.textProperty()

		));
		sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(daftarTable.comparatorProperty());
		daftarTable.setItems(sortedData);
	}

	@FXML
	private void handleAddButton() {
		this.mainApp.showAddDataDaftarScreen(CreateNomorDO());
	}

	@FXML
	private void handleEditButton() {
		int selectedIndex = daftarTable.getSelectionModel().getSelectedIndex();
		Daftar daftar = daftarTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			this.mainApp.showEditDataDaftarScreen(daftar, this.daftarTable);
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleDeleteButton() {
		int selectedIndex = daftarTable.getSelectionModel().getSelectedIndex();
		int filterIndex = sortedData.getSourceIndexFor(mainApp.getDaftarList(), selectedIndex);
		Daftar daftar = daftarTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Delete Data", "Delete Data",
					"Are you sure to delete data from index number: " + Integer.toString(selectedIndex + 1) + " ?");
			if (result.get() == ButtonType.OK) {
				String queryForDeleteData = "DELETE from daftar where idDaftar = '" + daftar.getIdDaftarValue() + "'";
				DatabaseHelper.queryToDatabase(queryForDeleteData);
				daftar.getCustomer().getDaftar().remove(daftar);
				daftar.getStaff().getDaftar().remove(daftar);
				mainApp.getDaftarList().remove(filterIndex);
			}
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	private String CreateNomorDO() {
		return "" + LocalDate.now().getMonthValue() + "" + LocalDate.now().getYear() + "-"
				+ (this.mainApp.getDaftarList().size() + 1) + "-SUB";
	}
}
