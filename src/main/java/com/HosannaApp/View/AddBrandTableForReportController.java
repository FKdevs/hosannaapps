package com.HosannaApp.View;

import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Brand;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddBrandTableForReportController {
	private Stage dialogStage;
	private MainApp mainApp;
	private ObservableList<Brand> brandTableData = FXCollections.observableArrayList();
	private ObservableList<Brand> brandTableDataToAdd = FXCollections.observableArrayList();
	
	@FXML
	private TableView<Brand> brandTable;
	@FXML
	private TableColumn<Brand, String> namaPemilikColumn;
	@FXML
	private TableColumn<Brand, String> merekColumn;
	@FXML
	private TableColumn<Brand, String> kelasColumn;
	@FXML
	private TableColumn<Brand, String> indexColumn;
	@FXML
	private TextField namaPemilikFieldFilter;
	@FXML
	private TextField merekFieldFilter;
	@FXML
	private TextField kelasFieldFilter;
	
	private ObservableList<Brand> brandTemp;
	private SortedList<Brand> sortedData;
	
	@FXML
	private void initialize() {
		indexColumn.setCellFactory(cellData -> {
			TableCell<Brand, String> indexCell = new TableCell<>();
			indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
					.otherwise(indexCell.indexProperty().add(1).asString()));
			return indexCell;
		});
		namaPemilikColumn.setCellValueFactory(cellData -> cellData.getValue().getCustomerParent().getNamaProperty());
		merekColumn.setCellValueFactory(cellData -> cellData.getValue().getMerek());
		kelasColumn.setCellValueFactory(cellData -> cellData.getValue().getKelas());
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		FilteredList<Brand> filteredData = new FilteredList<>(this.mainApp.getBrandList(), p -> true);
		filteredData.predicateProperty()
				.bind(Bindings.createObjectBinding(() -> brand -> brand.getMerekValue().toLowerCase()
						.contains(merekFieldFilter.getText().toLowerCase())
						&& brand.getKelasValue().toLowerCase().contains(kelasFieldFilter.getText().toLowerCase())
						&& brand
								.getCustomerParent().getNama().toLowerCase()
								.contains(namaPemilikFieldFilter.getText().toLowerCase()),
						namaPemilikFieldFilter.textProperty(), merekFieldFilter.textProperty(), kelasFieldFilter.textProperty()

		));
		sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(brandTable.comparatorProperty());
		brandTable.setItems(sortedData);
		brandTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE); 
	}
	
	public void setBrandListToAdd(ObservableList<Brand> brandTableData) {
		this.brandTableDataToAdd = brandTableData;
	}
	
	@FXML
	private void handleAdd() {
		brandTemp = brandTable.getSelectionModel().getSelectedItems();
		for(Brand brand : brandTemp) {
			brandTableDataToAdd.add(brand);
		}
	}
}
