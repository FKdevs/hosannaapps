package com.HosannaApp.View;

import com.HosannaApp.MainApp;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class MainScreenController {
    @FXML
    private Button periksaButton;
    @FXML
    private Button databaseMerekButton;
    @FXML
    private Button customerButton;
    @FXML
    private Button konsultanButton;
    @FXML
    private Button staffButton;

    private MainApp mainApp;

    public MainScreenController() {
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    private void handlePeriksaButton() {
        mainApp.showDaftarScreen();
    }
    
    @FXML
    private void handleCustomerButton() {
        mainApp.showCustomerScreen();
    }
    
    @FXML
    private void handleDatabaseMerekButton() {
        mainApp.showDatabaseMerekScreen();
    }
    
    @FXML
    private void handleKonsultanButton() {
    	mainApp.showKonsultanScreen();
    }
    
    @FXML
    private void handleStaffButton() {
        mainApp.showStaffScreen();
    }
}
