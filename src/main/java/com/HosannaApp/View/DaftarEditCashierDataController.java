package com.HosannaApp.View;

import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Util.AlertHelper;
import com.HosannaApp.Util.DateUtil;

import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DaftarEditCashierDataController {
	private MainApp mainApp;
	private Stage dialogStage;
	@FXML
	private DatePicker tanggalLunasField;
	@FXML
	private TextField hargaField;
	@FXML
	private TextField invoiceField;
	@FXML
	private TextField caraBayarField;
	private Daftar daftar;

	@FXML
	private void initialize() {
		tanggalLunasField.setConverter(DateUtil.getDateConverter());
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setDaftarToEdit(Daftar daftar) {
		this.daftar = daftar;
		restartInputForm();
	}

	@FXML
	private void handleConfirm() {
		Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
				"Are you sure to edit data ?");
		if (result.get() == ButtonType.OK) {
			this.daftar.setTanggalLunas(this.tanggalLunasField.getValue());
			this.daftar.setHarga(this.hargaField.getText());
			this.daftar.setInvoice(this.invoiceField.getText());
			this.daftar.setCaraBayar(this.caraBayarField.getText());

			DatabaseExecutor.updateDaftarToDatabase(daftar);
		}
		this.dialogStage.close();
	}

	@FXML
	private void handleClear() {
		restartInputForm();
	}

	private void restartInputForm() {
		this.tanggalLunasField.setValue(
				daftar.getTanggalLunasAsLocalDate().getYear() == 1980 ? null : daftar.getTanggalLunasAsLocalDate());
		this.hargaField.setText(daftar.getHargaValue());
		this.invoiceField.setText(daftar.getInvoiceValue());
		this.caraBayarField.setText(daftar.getCaraBayarValue());
	}
}
