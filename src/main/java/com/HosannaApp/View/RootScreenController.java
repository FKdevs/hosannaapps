package com.HosannaApp.View;

import com.HosannaApp.MainApp;

import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

public class RootScreenController {
    private MainApp mainApp;
    @FXML
    private MenuItem backMenu;
    @FXML
    private MenuItem quickAddCustomer;
    @FXML
    private MenuItem quickAddKonsultan;
    
    @FXML
    private void initialize() {
    }
    
    public MainApp getMainApp() {
        return mainApp;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    } 
    
    @FXML
    private void handleBackMenu() {
        this.mainApp.backToMainScreen();
    }
    
    @FXML
    private void handleQuickAddCustomer() {
    	this.mainApp.showQuickCustomerDataScreenAsDialog();
    }
    
    @FXML
    private void handleQuickAddKonsultan() {
    	this.mainApp.showKonsultanScreenAsDialog();
    }
}
