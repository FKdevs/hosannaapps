package com.HosannaApp.View;

import java.sql.SQLException;
import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.Staff;
import com.HosannaApp.Util.AlertHelper;

import javafx.beans.binding.Bindings;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class StaffDataScreenController {

	private MainApp mainApp;
	@FXML
	private TextField namaField;
	@FXML
	private TextField cabangField;
	@FXML
	private TableView<Staff> staffTable;
	@FXML
	private TableColumn<Staff, String> nomorColumn;
	@FXML
	private TableColumn<Staff, String> namaColumn;
	@FXML
	private TableColumn<Staff, String> cabangColumn;
	@FXML
	private TextField namaFieldFilter;
	@FXML
	private TextField cabangFieldFilter;
	@FXML
	private SortedList<Staff> sortedData;
	private Staff staffTemp;
	private int staffTempIndex;

	public StaffDataScreenController() {
	}

	@FXML
	private void initialize() {
		nomorColumn.setCellFactory(cellData -> {
			TableCell<Staff, String> indexCell = new TableCell<>();
			indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
					.otherwise(indexCell.indexProperty().add(1).asString()));
			return indexCell;
		});
		namaColumn.setCellValueFactory(cellData -> cellData.getValue().getNamaProperty());
		cabangColumn.setCellValueFactory(cellData -> cellData.getValue().getCabangProperty());
		
		staffTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		FilteredList<Staff> filteredData = new FilteredList<>(mainApp.getStaffList(), p -> true);
		filteredData.predicateProperty()
				.bind(Bindings.createObjectBinding(() -> konsultan -> konsultan.getNamaValue().toLowerCase()
						.contains(namaFieldFilter.getText().toLowerCase())
						&& konsultan.getCabangValue().toLowerCase().contains(cabangFieldFilter.getText().toLowerCase()),

						namaFieldFilter.textProperty(), cabangFieldFilter.textProperty()

		));
		sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(staffTable.comparatorProperty());
		staffTable.setItems(sortedData);
	}

	@FXML
	private void handleTableSelect() {
		staffTemp = staffTable.getSelectionModel().getSelectedItem();
		staffTempIndex = staffTable.getSelectionModel().getSelectedIndex();
		if (staffTemp != null) {
			namaField.setText(staffTemp.getNamaValue());
			cabangField.setText(staffTemp.getCabangValue());
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleAdd() {
		if (inputKonsultanCheck()) {
			Staff staff = new Staff(null, namaField.getText(), cabangField.getText());
			this.mainApp.addStaffList(staff);

			try {
				DatabaseExecutor.addNewStaffToDatabase(staff);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			this.clearInputForm();
		}
	}

	@FXML
	private void handleEdit() {
		int selectedIndex = staffTable.getSelectionModel().getSelectedIndex();
		Staff staff = staffTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			if (inputKonsultanCheck()) {
				Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
						"Are you sure to edit data from index number: " + Integer.toString(staffTempIndex + 1) + " ?");
				if (result.get() == ButtonType.OK) {
					staff.setNama(namaField.getText());
					staff.setCabang(cabangField.getText());
					DatabaseExecutor.updateStaffToDatabase(staff);
					staffTable.refresh();
					this.clearInputForm();
				} else {
					this.clearInputForm();
				}
			}
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleDelete() {
		int selectedIndex = staffTable.getSelectionModel().getSelectedIndex();
		Staff staff = staffTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Delete Data", "Delete Data",
					"Are you sure to delete data from index number: " + Integer.toString(staffTempIndex + 1) + " ?");
			if (result.get() == ButtonType.OK) {
				deleteStaffInDatabase(staff);
				this.clearInputForm();
			} else {
				this.clearInputForm();
			}
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleClear() {
		clearInputForm();
	}

	private void clearInputForm() {
		namaField.clear();
		cabangField.clear();
	}

	private boolean inputKonsultanCheck() {
		if (namaField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Nama Field", "Nama Field Kosong");
			return false;
		}
		return true;
	}

	private void deleteStaffInDatabase(Staff staff) {
		DatabaseExecutor.deleteStaffInDatabase(staff);
		for (Daftar daftar : staff.getDaftar()) {
			daftar.setStaff(new Staff("0"));
			DatabaseExecutor.updateStaffToDatabase(staff);
		}
		this.mainApp.getStaffList().remove(staff);
	}

}
