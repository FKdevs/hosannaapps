package com.HosannaApp.View;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Brand;
import com.HosannaApp.Model.Konsultan;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Util.AlertHelper;
import com.HosannaApp.Util.AutoCompleteComboBoxListener;
import com.HosannaApp.Util.DateUtil;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class DatabaseMerekDataScreenController {
	private MainApp mainApp;
	private Stage mainStage;
	@FXML
	private ComboBox<String> customerField;
	@FXML
	private TextField merekField;
	@FXML
	private TextField kelasField;
	@FXML
	private ComboBox<String> statusField;
	@FXML
	private DatePicker tanggalPendaftaranField;
	@FXML
	private DatePicker tanggalPengumumanField;
	@FXML
	private DatePicker tanggalKepemilikanField;
	@FXML
	private DatePicker tanggalKadarluarsaField;
	@FXML
	private ComboBox<String> konsultanField;
	@FXML
	private TextField deskripsiBarangField;
	@FXML
	private TableView<Brand> brandTable;
	@FXML
	private TableColumn<Brand, String> nomorColumn;
	@FXML
	private TableColumn<Brand, String> customerColumn;
	@FXML
	private TableColumn<Brand, String> merekColumn;
	@FXML
	private TableColumn<Brand, String> kelasColumn;
	@FXML
	private TableColumn<Brand, String> statusColumn;
	@FXML
	private TableColumn<Brand, String> tanggalPendaftaranColumn;
	@FXML
	private TableColumn<Brand, String> tanggalPengumumanColumn;
	@FXML
	private TableColumn<Brand, String> tanggalKepemilikanColumn;
	@FXML
	private TableColumn<Brand, String> tanggalKadarluarsaColumn;
	@FXML
	private TableColumn<Brand, String> konsultanColumn;
	@FXML
	private TableColumn<Brand, String> deskripsiBarangColumn;
	@FXML
	private TableColumn<Brand, ImageView> imageColumn;
	@FXML
	private TextField customerFieldFilter;
	@FXML
	private TextField merekFieldFilter;
	@FXML
	private TextField kelasFieldFilter;
	@FXML
	private TextField statusFieldFilter;
	@FXML
	private TextField tanggalPendaftaranFilter;
	@FXML
	private TextField tanggalPengumumanFilter;
	@FXML
	private TextField tanggalKepemilikanFilter;
	@FXML
	private TextField tanggalKadarluarsaFilter;
	@FXML
	private TextField konsultanFilter;
	@FXML
	private TextField deskripsiBarangFilter;
	@FXML
	private Button imageChooserButton;
	@FXML
	private ContextMenu updateStatusContextMenu;
	@FXML
	private ContextMenu updatePembayaranContextMenu;
	private SortedList<Brand> sortedData;
	private Brand brandTemp;
	private int brandTempIndex;
	private Image image;
	private ObservableList<String> customerDataInString;
	private ObservableList<String> konsultanDataInString;
	private ObservableList<String> optionsStatus = FXCollections.observableArrayList("Didaftar", "Dalam Proses",
			"Ditarik Kembali", "Ditolak", "Dibatalkan", "Dihapus", "Berakhir");

	public DatabaseMerekDataScreenController() {
	}

	@FXML
	private void initialize() {
		tanggalPendaftaranField.setConverter(DateUtil.getDateConverter());
		tanggalPengumumanField.setConverter(DateUtil.getDateConverter());
		tanggalKepemilikanField.setConverter(DateUtil.getDateConverter());
		tanggalKadarluarsaField.setConverter(DateUtil.getDateConverter());

		nomorColumn.setCellFactory(cellData -> {
			TableCell<Brand, String> indexCell = new TableCell<>();
			indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
					.otherwise(indexCell.indexProperty().add(1).asString()));
			return indexCell;
		});
		customerColumn.setCellValueFactory(cellData -> cellData.getValue().getCustomerParent().getNamaAlamatProperty());
		merekColumn.setCellValueFactory(cellData -> cellData.getValue().getMerek());
		kelasColumn.setCellValueFactory(cellData -> cellData.getValue().getKelas());
		statusColumn.setCellValueFactory(cellData -> cellData.getValue().getStatus());
		deskripsiBarangColumn.setCellValueFactory(cellData -> cellData.getValue().getDeskripsiBarang());
		tanggalPendaftaranColumn.setCellValueFactory(cellData -> cellData.getValue().getTanggalPendaftaranProperty());
		tanggalPengumumanColumn.setCellValueFactory(cellData -> cellData.getValue().getTanggalPengumumanProperty());
		tanggalKepemilikanColumn.setCellValueFactory(cellData -> cellData.getValue().getTanggalKepemilikanProperty());
		tanggalKadarluarsaColumn.setCellValueFactory(cellData -> cellData.getValue().getTanggalKadarluarsaProperty());
		konsultanColumn.setCellValueFactory(cellData -> cellData.getValue().getKonsultan().getNamaProperty());
		imageColumn.setCellValueFactory(cellData -> cellData.getValue().getImageViewProperty());

		brandTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		customerDataInString = FXCollections.observableArrayList();
		konsultanDataInString = FXCollections.observableArrayList();
		FilteredList<Brand> filteredData = new FilteredList<>(mainApp.getBrandList(), p -> true);
		filteredData.predicateProperty().bind(Bindings.createObjectBinding(
				() -> brand -> brand.getMerekValue().toLowerCase().contains(merekFieldFilter.getText().toLowerCase())
						&& brand.getKelasValue().toLowerCase().contains(kelasFieldFilter.getText().toLowerCase())
						&& brand.getCustomerParent().getNama().toLowerCase()
								.contains(customerFieldFilter.getText().toLowerCase())
						&& brand.getTanggalPendaftaran().toLowerCase()
								.contains(tanggalPendaftaranFilter.getText().toLowerCase())
						&& brand.getStatusValue().toLowerCase().contains(statusFieldFilter.getText().toLowerCase())
						&& brand.getTanggalPengumuman().toLowerCase()
								.contains(tanggalPengumumanFilter.getText().toLowerCase())
						&& brand.getTanggalKepemilikan().toLowerCase()
								.contains(tanggalKepemilikanFilter.getText().toLowerCase())
						&& brand.getTanggalKadarluarsa().toLowerCase()
								.contains(tanggalKadarluarsaFilter.getText().toLowerCase())
						&& brand.getDeskripsiBarangValue().toLowerCase()
								.contains(deskripsiBarangFilter.getText().toLowerCase()),

				merekFieldFilter.textProperty(), kelasFieldFilter.textProperty(), statusFieldFilter.textProperty(),
				customerFieldFilter.textProperty(), tanggalPendaftaranFilter.textProperty(),
				tanggalPengumumanFilter.textProperty(), tanggalKepemilikanFilter.textProperty(),
				tanggalKadarluarsaFilter.textProperty(), deskripsiBarangFilter.textProperty()));
		sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(brandTable.comparatorProperty());
		brandTable.setItems(sortedData);
		brandTable.refresh();
		for (Customer customer : mainApp.getCustomerList()) {
			customerDataInString.add(customer.getNama() + "|" + customer.getAlamat());
		}
		for (Konsultan konsultan : mainApp.getKonsultanList()) {
			konsultanDataInString.add(konsultan.getNamaValue());
		}
		konsultanField.setItems(konsultanDataInString);
		new AutoCompleteComboBoxListener(konsultanField);
		customerField.setItems(customerDataInString);
		new AutoCompleteComboBoxListener(customerField);
		statusField.setItems(optionsStatus);
	}

	public void setMainStage(Stage mainStage) {
		this.mainStage = mainStage;
	}

	@FXML
	private void handleImageChooser() {
		FileChooser fileChooser = new FileChooser();
		ExtensionFilter filter = new ExtensionFilter("Image Files", "*.jpg", "*.png", "*.gif", "*.jpeg");
		fileChooser.setTitle("Open Image");
		fileChooser.getExtensionFilters().add(filter);
		File file = fileChooser.showOpenDialog(mainStage);
		InputStream fileStream;
		try {
			fileStream = new FileInputStream(file);
			image = new Image(fileStream);
			this.imageChooserButton.setText(file.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void handleTableSelect() {
		brandTemp = brandTable.getSelectionModel().getSelectedItem();
		brandTempIndex = brandTable.getSelectionModel().getSelectedIndex();
		if (brandTemp != null) {
			customerField.setValue(
					brandTemp.getCustomerParent().getNama() + "|" + brandTemp.getCustomerParent().getAlamat());
			merekField.setText(brandTemp.getMerekValue());
			kelasField.setText(brandTemp.getKelasValue());
			statusField.setValue(brandTemp.getStatusValue());
			deskripsiBarangField.setText(brandTemp.getDeskripsiBarangValue());
			tanggalPendaftaranField.setValue(brandTemp.getTanggalPendaftaranAsLocalDate().getYear() == 1980 ? null
					: brandTemp.getTanggalPendaftaranAsLocalDate());
			tanggalPengumumanField.setValue(brandTemp.getTanggalPengumumanAsLocalDate().getYear() == 1980 ? null
					: brandTemp.getTanggalPengumumanAsLocalDate());
			tanggalKepemilikanField.setValue(brandTemp.getTanggalKepemilikanAsLocalDate().getYear() == 1980 ? null
					: brandTemp.getTanggalKepemilikanAsLocalDate());
			tanggalKadarluarsaField.setValue(brandTemp.getTanggalKadarluarsaAsLocalDate().getYear() == 1980 ? null
					: brandTemp.getTanggalKadarluarsaAsLocalDate());
			konsultanField.setValue(brandTemp.getKonsultan().getNamaValue());
		} else {
			// AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please
			// select a data in the table.");
		}
	}

	@FXML
	private void handleTanggalKepemilikanDatePicker() {
		if (tanggalKepemilikanField.getValue() != null) {
			LocalDate tanggalKepemilikanTemp = tanggalKepemilikanField.getValue();
			LocalDate tanggalKadarluarsa = LocalDate.of(tanggalKepemilikanTemp.getYear() + 10,
					tanggalKepemilikanTemp.getMonthValue(), tanggalKepemilikanTemp.getDayOfMonth());
			tanggalKadarluarsaField.setValue(tanggalKadarluarsa);
		}
	}

	@FXML
	private void handleAdd() {
		if (inputBrandCheck()) {
			Customer customerTemp = searchCustomer(customerField.getValue().split("\\|")[0]);
			ImageView imageMerek = new ImageView(image);
			Brand brand = new Brand("", customerTemp, merekField.getText(), kelasField.getText(),
					statusField.getValue(), tanggalPendaftaranField.getValue(), tanggalPengumumanField.getValue(),
					tanggalKepemilikanField.getValue(), tanggalKadarluarsaField.getValue(),
					deskripsiBarangField.getText(), searchKonsultan(konsultanField.getValue()), imageMerek);
			this.mainApp.addBrandList(brand);
			customerTemp.addBrand(brand);

			try {
				DatabaseExecutor.addNewBrandToDatabase(brand);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			this.clearInputForm();
		}
	}

	@FXML
	private void handleEdit() {
		int selectedIndex = brandTable.getSelectionModel().getSelectedIndex();
		Brand brand = brandTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			if (inputBrandCheck()) {
				Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
						"Are you sure to edit data from index number: " + Integer.toString(brandTempIndex + 1) + " ?");
				if (result.get() == ButtonType.OK) {
					brand.setKelas(kelasField.getText());
					brand.setDeskripsiBarang(deskripsiBarangField.getText());
					brand.setMerek(merekField.getText());
					brand.setKonsultan(searchKonsultan(konsultanField.getValue()));
					brand.setCustomerParent(searchCustomer(customerField.getValue().split("\\|")[0]));
					brand.setStatus(statusField.getValue());
					brand.setTanggalPendaftaran(tanggalPendaftaranField.getValue());
					brand.setTanggalPengumuman(tanggalPengumumanField.getValue());
					brand.setTanggalKepemilikan(tanggalKepemilikanField.getValue());
					brand.setTanggalKadarluarsa(tanggalKadarluarsaField.getValue());
					brand.setImage(new ImageView(image));
					DatabaseExecutor.updateBrandToDatabase(brand);

					brandTable.refresh();
					this.clearInputForm();
				} else {
					this.clearInputForm();
				}
			}
		} else {
			// AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please
			// select a data in the table.");
		}
	}

	@FXML
	private void handleDelete() {
		int selectedIndex = brandTable.getSelectionModel().getSelectedIndex();
		int filterIndex = sortedData.getSourceIndexFor(mainApp.getBrandList(), selectedIndex);
		Brand brand = brandTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Delete Data", "Delete Data",
					"Are you sure to delete data from index number: " + Integer.toString(selectedIndex + 1) + " ?");
			if (result.get() == ButtonType.OK) {
				DatabaseExecutor.deleteBrandInDatabase(brand);
				brand.getCustomerParent().getBrand().remove(brand);
				brand.getKonsultan().getBrand().remove(brand);
				mainApp.getBrandList().remove(filterIndex);
				brandTable.refresh();
				this.clearInputForm();
			}
		} else {
			// AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please
			// select a data in the table.");
		}
	}

	@FXML
	private void handleClear() {
		clearInputForm();
	}

	private void clearInputForm() {
		customerField.setValue("");
		merekField.clear();
		kelasField.clear();
		statusField.setValue("");
		tanggalPendaftaranField.setValue(null);
		tanggalPengumumanField.setValue(null);
		tanggalKepemilikanField.setValue(null);
		tanggalKadarluarsaField.setValue(null);
		konsultanField.setValue("");
		deskripsiBarangField.clear();
		imageChooserButton.setText("Choose File");
		image = null;
	}

	private boolean inputBrandCheck() {
		Customer searchCustomerTemp = searchCustomer(customerField.getValue().split("\\|")[0]);
		if (customerField.getSelectionModel().isEmpty() || searchCustomerTemp == null) {
			AlertHelper.showErrorWarning("Data Kurang", "Nama Customer Field", "Nama Customer Field Kosong/Invalid");
			return false;
		}
		if (merekField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Merek Field", "Merek Field Kosong");
			return false;
		}
		if (konsultanField.getValue().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Konsultan Field", "Konsultan Field Kosong");
			return false;
		}
		return true;
	}

	private Customer searchCustomer(String namaCustomer) {
		for (Customer customer : mainApp.getCustomerList()) {
			if (customer.getNama().equals(namaCustomer)) {
				return customer;
			}
		}
		return null;
	}

	private Konsultan searchKonsultan(String namaKonsultan) {
		for (Konsultan konsultan : mainApp.getKonsultanList()) {
			if (konsultan.getNamaValue().equals(namaKonsultan)) {
				return konsultan;
			}
		}
		return null;
	}
}
