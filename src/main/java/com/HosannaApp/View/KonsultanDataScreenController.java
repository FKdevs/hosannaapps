package com.HosannaApp.View;

import java.sql.SQLException;
import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Brand;
import com.HosannaApp.Model.Konsultan;
import com.HosannaApp.Util.AlertHelper;

import javafx.beans.binding.Bindings;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class KonsultanDataScreenController {
	private MainApp mainApp;
	@FXML
	private TextField namaField;
	@FXML
	private TextArea alamatField;
	@FXML
	private TextField kewarganegaraanField;
	@FXML
	private TableView<Konsultan> konsultanTable;
	@FXML
	private TableColumn<Konsultan, String> nomorColumn;
	@FXML
	private TableColumn<Konsultan, String> namaColumn;
	@FXML
	private TableColumn<Konsultan, String> alamatColumn;
	@FXML
	private TableColumn<Konsultan, String> kewarganegaraanColumn;
	@FXML
	private TextField namaFieldFilter;
	@FXML
	private TextField alamatFieldFilter;
	@FXML
	private TextField kewarganegaraanFieldFilter;
	@FXML
	private SortedList<Konsultan> sortedData;
	private Konsultan konsultanTemp;
	private int konsultanTempIndex;

	public KonsultanDataScreenController() {
	}

	@FXML
	private void initialize() {
		nomorColumn.setCellFactory(cellData -> {
			TableCell<Konsultan, String> indexCell = new TableCell<>();
			indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
					.otherwise(indexCell.indexProperty().add(1).asString()));
			return indexCell;
		});
		namaColumn.setCellValueFactory(cellData -> cellData.getValue().getNamaProperty());
		alamatColumn.setCellValueFactory(cellData -> cellData.getValue().getAlamatProperty());
		kewarganegaraanColumn.setCellValueFactory(cellData -> cellData.getValue().getKewarganegaraanProperty());
		
		konsultanTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		FilteredList<Konsultan> filteredData = new FilteredList<>(mainApp.getKonsultanList(), p -> true);
		filteredData.predicateProperty()
				.bind(Bindings.createObjectBinding(() -> konsultan -> konsultan.getNamaValue().toLowerCase()
						.contains(namaFieldFilter.getText().toLowerCase())
						&& konsultan.getAlamatValue().toLowerCase().contains(alamatFieldFilter.getText().toLowerCase())
						&& konsultan.getKewarganegaraanValue().toLowerCase()
								.contains(kewarganegaraanFieldFilter.getText().toLowerCase()),

						namaFieldFilter.textProperty(), alamatFieldFilter.textProperty()

		));
		sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(konsultanTable.comparatorProperty());
		konsultanTable.setItems(sortedData);
	}

	@FXML
	private void handleTableSelect() {
		konsultanTemp = konsultanTable.getSelectionModel().getSelectedItem();
		konsultanTempIndex = konsultanTable.getSelectionModel().getSelectedIndex();
		if (konsultanTemp != null) {
			namaField.setText(konsultanTemp.getNamaValue());
			alamatField.setText(konsultanTemp.getAlamatValue());
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleAdd() {
		if (inputKonsultanCheck()) {
			Konsultan konsultan = new Konsultan(null, namaField.getText(), alamatField.getText(),
					kewarganegaraanField.getText());
			this.mainApp.addKonsultanList(konsultan);

			try {
				DatabaseExecutor.addNewKonsultanToDatabase(konsultan);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			this.clearInputFormAndRefreshList();
		}
	}

	@FXML
	private void handleEdit() {
		int selectedIndex = konsultanTable.getSelectionModel().getSelectedIndex();
		Konsultan konsultan = konsultanTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			if (inputKonsultanCheck()) {
				Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
						"Are you sure to edit data from index number: " + Integer.toString(konsultanTempIndex + 1)
								+ " ?");
				if (result.get() == ButtonType.OK) {
					konsultan.setNama(namaField.getText());
					konsultan.setAlamat(alamatField.getText());
					konsultan.setKewarganegaraan(kewarganegaraanField.getText());

					DatabaseExecutor.updateKonsultanToDatabase(konsultan);

					konsultanTable.refresh();
					this.clearInputFormAndRefreshList();
				} else {
					this.clearInputFormAndRefreshList();
				}
			}
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleDelete() {

		int selectedIndex = konsultanTable.getSelectionModel().getSelectedIndex();
		Konsultan konsultan = konsultanTable.getSelectionModel().getSelectedItem();
		if (selectedIndex >= 0) {
			Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Delete Data", "Delete Data",
					"Are you sure to delete data from index number: " + Integer.toString(konsultanTempIndex + 1)
							+ " ?");
			if (result.get() == ButtonType.OK) {
				deleteKonsultanInDatabase(konsultan);
				this.clearInputFormAndRefreshList();
			} else {
				this.clearInputFormAndRefreshList();
			}
		} else {
			//AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
		}
	}

	@FXML
	private void handleClear() {
		clearInputFormAndRefreshList();
	}

	private void clearInputFormAndRefreshList() {
		namaField.clear();
		alamatField.clear();
		this.mainApp.refreshCustomerAndKonsultanListInDatabaseMerek();
	}

	private boolean inputKonsultanCheck() {
		if (namaField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Nama Field", "Nama Field Kosong");
			return false;
		}
		return true;
	}

	private void deleteKonsultanInDatabase(Konsultan konsultan) {
		DatabaseExecutor.deleteKonsultanInDatabase(konsultan);
		for (Brand brand : konsultan.getBrand()) {
			brand.setKonsultan(new Konsultan("0", "-"));
			DatabaseExecutor.updateBrandToDatabase(brand);
			this.mainApp.getKonsultanList().remove(konsultan);
		}
	}

	/**
	 * Replace Konsultan value of Brand and Periksa which the origin konsultan is
	 * deleted.
	 * 
	 * @param konsultanToDelete
	 */
	private void replaceKonsultanForBrandAndPeriksaWith(Konsultan konsultanToDelete) {
		for (Brand brand : this.mainApp.getBrandList()) {
			if (brand.getKonsultan().equals(konsultanToDelete)) {
				brand.setKonsultan(new Konsultan("0", "-"));
				DatabaseExecutor.updateBrandToDatabase(brand);
			}
		}
	}
}
