package com.HosannaApp.View;

import com.HosannaApp.MainApp;
import com.HosannaApp.Util.AlertHelper;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class LoginPageController {
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private Button confirm;

    private Stage primaryStage;
    @FXML
    private AnchorPane anchorPane;
    private MainApp mainApp;

    public LoginPageController() {
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    private void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (isInputValid()) {
                mainApp.showMainScreen();
            } else {
                AlertHelper.showErrorWarning("Invalid Fields", "Please correct invalid fields",
                        "Username/Password Salah");
            }
        }
    }

    @FXML
    private void handleConfirm() {
        if (isInputValid()) {
            mainApp.showMainScreen();
        } else {
            AlertHelper.showErrorWarning("Invalid Fields", "Please correct invalid fields", "Username/Password Salah");
        }
    }

    private Boolean isInputValid() {
        return username.getText().equals("admin") && password.getText().equals("admin");
    }
}
