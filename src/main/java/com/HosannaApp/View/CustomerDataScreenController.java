package com.HosannaApp.View;

import java.sql.SQLException;
import java.util.Optional;

import com.HosannaApp.MainApp;
import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.Model.Brand;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Util.AlertHelper;

import javafx.beans.binding.Bindings;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonType;

public class CustomerDataScreenController {
    private MainApp mainApp;
    @FXML
    private TextField namaField;
    @FXML
    private TextArea alamatField;
    @FXML
    private TextField alamatKirimField;
    @FXML
    private TextField provinsiField;
    @FXML
    private TextField kotaField;
    @FXML
    private TextField kodePosField;
    @FXML
    private TextField teleponField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField kewarganegaraanField;
    @FXML
    private TableView<Customer> customerDataAddEditTable;
    @FXML
    private TableColumn<Customer, String> nomorColumn;
    @FXML
    private TableColumn<Customer, String> namaColumn;
    @FXML
    private TableColumn<Customer, String> alamatColumn;
    @FXML
    private TableColumn<Customer, String> alamatKirimColumn;
    @FXML
    private TableColumn<Customer, String> provinsiColumn;
    @FXML
    private TableColumn<Customer, String> kotaColumn;
    @FXML
    private TableColumn<Customer, String> kodePosColumn;
    @FXML
    private TableColumn<Customer, String> teleponColumn;
    @FXML
    private TableColumn<Customer, String> emailColumn;
    @FXML
    private TableColumn<Customer, String> kewarganegaraanColumn;
    @FXML
    private TextField namaFieldFilter;
    @FXML
    private TextField alamatFieldFilter;
    @FXML
    private TextField alamatKirimFieldFilter;
    @FXML
    private TextField provinsiFieldFilter;
    @FXML
    private TextField kotaFieldFilter;
    @FXML
    private TextField kodePosFieldFilter;
    @FXML
    private TextField teleponFieldFilter;
    @FXML
    private TextField emailFieldFilter;
    @FXML
    private TextField kewarganegaraanFieldFilter;
    private SortedList<Customer> sortedData;
    private Customer customerTemp;
    private int customerTempIndex;

    public CustomerDataScreenController() {
    }

    @FXML
    private void initialize() {
        nomorColumn.setCellFactory(cellData -> {
            TableCell<Customer, String> indexCell = new TableCell<>();
            indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
                    .otherwise(indexCell.indexProperty().add(1).asString()));
            return indexCell;
        });
        namaColumn.setCellValueFactory(cellData -> cellData.getValue().getNamaProperty());
        alamatColumn.setCellValueFactory(cellData -> cellData.getValue().getAlamatProperty());
        alamatKirimColumn.setCellValueFactory(cellData -> cellData.getValue().getAlamatKirimProperty());
        provinsiColumn.setCellValueFactory(cellData -> cellData.getValue().getProvinsiProperty());
        kotaColumn.setCellValueFactory(cellData -> cellData.getValue().getKotaProperty());
        kodePosColumn.setCellValueFactory(cellData -> cellData.getValue().getKodePosProperty());
        teleponColumn.setCellValueFactory(cellData -> cellData.getValue().getTeleponProperty());
        emailColumn.setCellValueFactory(cellData -> cellData.getValue().getEmailProperty());
        kewarganegaraanColumn.setCellValueFactory(cellData -> cellData.getValue().getKewarganegaraanProperty());
        
        customerDataAddEditTable.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        FilteredList<Customer> filteredData = new FilteredList<>(mainApp.getCustomerList(), p -> true);
        filteredData.predicateProperty().bind(Bindings.createObjectBinding(
                () -> customer -> customer.getNama().toLowerCase().contains(namaFieldFilter.getText().toLowerCase())
                        && customer.getAlamat().toLowerCase().contains(alamatFieldFilter.getText().toLowerCase())
                        && customer.getAlamatKirim().toLowerCase()
                                .contains(alamatKirimFieldFilter.getText().toLowerCase())
                        && customer.getProvinsi().toLowerCase().contains(provinsiFieldFilter.getText().toLowerCase())
                        && customer.getKota().toLowerCase().contains(kotaFieldFilter.getText().toLowerCase())
                        && customer.getKodePos().toLowerCase().contains(kodePosFieldFilter.getText().toLowerCase())
                        && customer.getTelepon().toLowerCase().contains(teleponFieldFilter.getText().toLowerCase())
                        && customer.getEmail().toLowerCase().contains(emailFieldFilter.getText().toLowerCase())
                        && customer.getKewarganegaraan().toLowerCase()
                                .contains(kewarganegaraanFieldFilter.getText().toLowerCase()),

                namaFieldFilter.textProperty(), alamatFieldFilter.textProperty(), alamatKirimFieldFilter.textProperty(),
                provinsiFieldFilter.textProperty(), kotaFieldFilter.textProperty(), kodePosFieldFilter.textProperty(),
                teleponFieldFilter.textProperty(), emailFieldFilter.textProperty(),
                kewarganegaraanFieldFilter.textProperty()

        ));
        sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(customerDataAddEditTable.comparatorProperty());
        customerDataAddEditTable.setItems(sortedData);
    }

    @FXML
    private void handleTableSelect() {
        customerTemp = customerDataAddEditTable.getSelectionModel().getSelectedItem();
        customerTempIndex = customerDataAddEditTable.getSelectionModel().getSelectedIndex();
        if (customerTemp != null) {
            namaField.setText(customerTemp.getNama());
            alamatField.setText(customerTemp.getAlamat());
            alamatKirimField.setText(customerTemp.getAlamatKirim());
            provinsiField.setText(customerTemp.getProvinsi());
            kotaField.setText(customerTemp.getKota());
            kodePosField.setText(customerTemp.getKodePos());
            teleponField.setText(customerTemp.getTelepon());
            emailField.setText(customerTemp.getEmail());
            kewarganegaraanField.setText(customerTemp.getKewarganegaraan());
        } else {
            //AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
        }
    }

    @FXML
    private void handleAdd() {
        if (inputCustomerCheck()) {
            Customer customer = new Customer(null, namaField.getText(), alamatField.getText(),
                    alamatKirimField.getText(), provinsiField.getText(), kotaField.getText(), kodePosField.getText(),
                    teleponField.getText(), emailField.getText(), kewarganegaraanField.getText());
            this.mainApp.addCustomerList(customer);
            
            try {
                DatabaseExecutor.addNewCustomerToDatabase(customer);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
            this.clearInputFormAndRefreshList();
        }
    }

    @FXML
    private void handleEdit() {
        int selectedIndex = customerDataAddEditTable.getSelectionModel().getSelectedIndex();
        Customer customer = customerDataAddEditTable.getSelectionModel().getSelectedItem();
        if (selectedIndex >= 0) {
            if (inputCustomerCheck()) {
                Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
                        "Are you sure to edit data from index number: " + Integer.toString(customerTempIndex + 1)
                                + " ?");
                if (result.get() == ButtonType.OK) {
                    customer.setNama(namaField.getText());
                    customer.setAlamat(alamatField.getText());
                    customer.setAlamatKirim(alamatKirimField.getText());
                    customer.setProvinsi(provinsiField.getText());
                    customer.setKota(kotaField.getText());
                    customer.setKodePos(kodePosField.getText());
                    customer.setTelepon(teleponField.getText());
                    customer.setEmail(emailField.getText());
                    customer.setKewarganegaraan(kewarganegaraanField.getText());
                    DatabaseExecutor.updateCustomerToDatabase(customer);
                    customerDataAddEditTable.refresh();
                    this.clearInputFormAndRefreshList();
                } else {
                    this.clearInputFormAndRefreshList();
                }
            }
        } else {
            //AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
        }
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = customerDataAddEditTable.getSelectionModel().getSelectedIndex();
        Customer customer = customerDataAddEditTable.getSelectionModel().getSelectedItem();
        if (selectedIndex >= 0) {
            Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Delete Data", "Delete Data",
                    "Are you sure to delete data from index number: " + Integer.toString(customerTempIndex + 1) + " ?");
            if (result.get() == ButtonType.OK) {
                deleteCustomerInDatabase(customer);
                this.clearInputFormAndRefreshList();
            } else {
                this.clearInputFormAndRefreshList();
            }
        } else {
            //AlertHelper.showErrorWarning("No Selection", "No Data Selection", "Please select a data in the table.");
        }
    }

    @FXML
    private void handleClear() {
        clearInputFormAndRefreshList();
    }

    private void clearInputFormAndRefreshList() {
        namaField.clear();
        alamatField.clear();
        alamatKirimField.clear();
        provinsiField.clear();
        kotaField.clear();
        kodePosField.clear();
        teleponField.clear();
        emailField.clear();
        kewarganegaraanField.clear();
        this.mainApp.refreshCustomerAndKonsultanListInDatabaseMerek();
    }

    private boolean inputCustomerCheck() {
        if (namaField.getText().isEmpty()) {
            AlertHelper.showErrorWarning("Data Kurang", "Nama Field", "Nama Field Kosong");
            return false;
        }
        if (alamatField.getText().isEmpty()) {
            AlertHelper.showErrorWarning("Data Kurang", "Alamat Field", "Alamat Field Kosong");
            return false;
        }
        return true;
    }

    private void deleteCustomerInDatabase(Customer customer) {
        DatabaseExecutor.deleteCustomerInDatabase(customer);
        for (Brand brand : customer.getBrand()) {
            brand.setCustomerParent(new Customer("0"));
            DatabaseExecutor.updateBrandToDatabase(brand);
        }
        for (Daftar daftar : customer.getDaftar()) {
        	daftar.setCustomer(new Customer("0"));
            DatabaseExecutor.updateDaftarToDatabase(daftar);
        }
        this.mainApp.getCustomerList().remove(customer);
    }
}
