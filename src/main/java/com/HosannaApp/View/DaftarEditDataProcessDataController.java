package com.HosannaApp.View;

import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.Staff;
import com.HosannaApp.Util.AlertHelper;
import com.HosannaApp.Util.AutoCompleteComboBoxListener;
import com.HosannaApp.Util.DateUtil;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DaftarEditDataProcessDataController {
	private MainApp mainApp;
	private Stage dialogStage;
	@FXML
	private ComboBox<String> staffField;
	@FXML
	private DatePicker tanggalKirimHasilField;
	@FXML
	private TextField saranField;
	@FXML
	private TextField caraKirimHasilField;
	private Daftar daftar;
	private TableView<Daftar> mainDaftarTable;
	private ObservableList<String> staffDataInString;

	@FXML
	private void initialize() {
		tanggalKirimHasilField.setConverter(DateUtil.getDateConverter());
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		staffDataInString = FXCollections.observableArrayList();

		for (Staff staff : mainApp.getStaffList()) {
			staffDataInString.add(staff.getNamaValue());
		}
		staffField.setItems(staffDataInString);
		new AutoCompleteComboBoxListener(staffField);
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setDaftarToEdit(Daftar daftar) {
		this.daftar = daftar;
		restartInputForm();
	}

	public void setMainDaftarTable(TableView<Daftar> mainDaftarTable) {
		this.mainDaftarTable = mainDaftarTable;
	}

	@FXML
	private void handleConfirm() {
		Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
				"Are you sure to edit data ?");
		if (result.get() == ButtonType.OK) {
			this.daftar.setTanggalKirimHasil(this.tanggalKirimHasilField.getValue());
			this.daftar.setSaran(this.saranField.getText());
			this.daftar.setCaraKirimHasil(this.caraKirimHasilField.getText());
			this.daftar.setStaff(searchStaff(staffField.getValue()));

			DatabaseExecutor.updateDaftarToDatabase(daftar);
			this.mainDaftarTable.refresh();
		}
		this.dialogStage.close();
	}

	@FXML
	private void handleClear() {
		restartInputForm();
	}

	private void restartInputForm() {
		this.tanggalKirimHasilField.setValue(daftar.getTanggalKirimHasilAsLocalDate().getYear() == 1980 ? null
				: daftar.getTanggalKirimHasilAsLocalDate());
		this.saranField.setText(daftar.getSaranValue());
		this.caraKirimHasilField.setText(daftar.getCaraKirimHasilValue());
		this.staffField.setValue(daftar.getStaff().getNamaValue());
	}

	private Staff searchStaff(String namaStaff) {
		for (Staff staff : mainApp.getStaffList()) {
			if (staff.getNamaValue().equals(namaStaff)) {
				return staff;
			}
		}
		return null;
	}
}
