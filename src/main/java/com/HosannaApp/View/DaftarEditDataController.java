package com.HosannaApp.View;

import java.time.LocalDate;
import java.util.Optional;

import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.Staff;
import com.HosannaApp.Util.AlertHelper;
import com.HosannaApp.Util.AutoCompleteComboBoxListener;
import com.HosannaApp.Util.DateUtil;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DaftarEditDataController {
	private MainApp mainApp;
	private Stage dialogStage;
	@FXML
	private ComboBox<String> customerField;
	@FXML
	private ComboBox<String> jenisPermintaanField;
	@FXML
	private ComboBox<String> staffField;
	@FXML
	private DatePicker tanggalOrderField;
	@FXML
	private TextField nomorDOField;
	@FXML
	private TextField namaMerekField;
	@FXML
	private TextField kelasField;
	@FXML
	private TextField keteranganOrderField;

	private Daftar daftar;
	private TableView<Daftar> mainDaftarTable;
	private ObservableList<String> customerDataInString;
	private ObservableList<String> staffDataInString;
	private ObservableList<String> optionsStatus = FXCollections.observableArrayList("Periksa", "Daftar Baru",
			"Perpanjang");

	@FXML
	private void initialize() {
		tanggalOrderField.setConverter(DateUtil.getDateConverter());
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		tanggalOrderField.setValue(LocalDate.now());
		customerDataInString = FXCollections.observableArrayList();
		staffDataInString = FXCollections.observableArrayList();
		for (Customer customer : mainApp.getCustomerList()) {
			customerDataInString.add(customer.getNama() + "|" + customer.getAlamat());
		}
		for (Staff staff : mainApp.getStaffList()) {
			staffDataInString.add(staff.getNamaValue());
		}
		staffField.setItems(staffDataInString);
		new AutoCompleteComboBoxListener(staffField);
		customerField.setItems(customerDataInString);
		new AutoCompleteComboBoxListener(customerField);
		jenisPermintaanField.setItems(optionsStatus);
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setDaftarToEdit(Daftar daftar) {
		this.daftar = daftar;
		this.nomorDOField.setText(daftar.getNomorDOValue());
		this.tanggalOrderField.setValue(daftar.getTanggalOrderAsLocalDate());
		String customerString = daftar.getCustomer().getNama() + "|" + daftar.getCustomer().getAlamat();
		this.customerField.setValue(customerString);
		this.jenisPermintaanField.setValue(daftar.getJenisPermintaanValue());
		this.staffField.setValue(daftar.getStaff().getNamaValue());
		this.namaMerekField.setText(daftar.getNamaMerekValue());
		this.kelasField.setText(daftar.getKelasValue());
		this.keteranganOrderField.setText(daftar.getKeteranganOrderValue());
	}
	
	public void setMainDaftarTable(TableView<Daftar> mainDaftarTable) {
		this.mainDaftarTable = mainDaftarTable;
	}

	@FXML
	private void handleConfirm() {
		Optional<ButtonType> result = AlertHelper.showConfirmationWarning("Edit Data", "Edit Data",
				"Are you sure to edit data ?");
		if (result.get() == ButtonType.OK) {
			if (inputDaftarCheck()) {
				Customer customerTemp = searchCustomer(customerField.getValue().split("\\|")[0]);
				this.daftar.setCustomer(customerTemp);
				this.daftar.setStaff(searchStaff(staffField.getValue()));
				this.daftar.setJenisPermintaan(jenisPermintaanField.getValue());
				this.daftar.setNamaMerek(namaMerekField.getText());
				this.daftar.setKelas(kelasField.getText());
				this.daftar.setKeteranganOrder(keteranganOrderField.getText());
				this.daftar.setTanggalOrder(tanggalOrderField.getValue());

				DatabaseExecutor.updateDaftarToDatabase(daftar);
				this.mainDaftarTable.refresh();
			}
		}
		this.dialogStage.close();
	}

	@FXML
	private void handleClear() {
		clearInputForm();
	}

	private void clearInputForm() {
		customerField.setValue("");
		namaMerekField.clear();
		kelasField.clear();
		jenisPermintaanField.setValue("");
		keteranganOrderField.clear();
		tanggalOrderField.setValue(LocalDate.now());
		staffField.setValue("");
	}

	private boolean inputDaftarCheck() {
		Customer searchCustomerTemp = searchCustomer(customerField.getValue().split("\\|")[0]);
		if (customerField.getSelectionModel().isEmpty() || searchCustomerTemp == null) {
			AlertHelper.showErrorWarning("Data Kurang", "Nama Customer Field", "Nama Customer Field Kosong/Invalid");
			return false;
		}
		if (jenisPermintaanField.getValue().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Jenis Permintaan Field", "Jenis Permintaan Field Kosong");
			return false;
		}
		if (namaMerekField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Merek Field", "Merek Field Kosong");
			return false;
		}
		if (staffField.getValue().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Staff Field", "Staff Field Kosong");
			return false;
		}
		return true;
	}

	private Customer searchCustomer(String namaCustomer) {
		for (Customer customer : mainApp.getCustomerList()) {
			if (customer.getNama().equals(namaCustomer)) {
				return customer;
			}
		}
		return null;
	}

	private Staff searchStaff(String namaStaff) {
		for (Staff staff : mainApp.getStaffList()) {
			if (staff.getNamaValue().equals(namaStaff)) {
				return staff;
			}
		}
		return null;
	}
}
