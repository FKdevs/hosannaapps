package com.HosannaApp.View;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.HosannaApp.MainApp;
import com.HosannaApp.PrintReport;
import com.HosannaApp.Model.Brand;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.ReportBrand;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReportDialogController {
	private Stage dialogStage;
	private MainApp mainApp;
	private Daftar daftar;
	private ObservableList<Brand> brandTableData = FXCollections.observableArrayList();
	
	@FXML
	private TableView<Brand> brandTable;
	@FXML
	private TableColumn<Brand, String> namaPemilikColumn;
	@FXML
	private TableColumn<Brand, String> merekColumn;
	@FXML
	private TableColumn<Brand, String> kelasColumn;
	@FXML
	private TableColumn<Brand, String> indexColumn;
	@FXML
	private TextField namaPemohonField;
	@FXML
	private TextField merekField;
	@FXML
	private TextField kelasField;
	@FXML
	private TextArea kesimpulanField;
	@FXML
	private ComboBox<String> saranField;
	
	@FXML
	private void initialize() {
		kesimpulanField.clear();
		ObservableList<String> options = 
			    FXCollections.observableArrayList(
			        "Dapat Didaftarkan",
			        "50%",
			        "Tidak Dapat Didaftarkan"
			    );
		saranField.setItems(options);
		indexColumn.setCellFactory(cellData -> {
			TableCell<Brand, String> indexCell = new TableCell<>();
			indexCell.textProperty().bind(Bindings.when(indexCell.emptyProperty()).then("")
					.otherwise(indexCell.indexProperty().add(1).asString()));
			return indexCell;
		});
		namaPemilikColumn.setCellValueFactory(cellData -> cellData.getValue().getCustomerParent().getNamaProperty());
		merekColumn.setCellValueFactory(cellData -> cellData.getValue().getMerek());
		kelasColumn.setCellValueFactory(cellData -> cellData.getValue().getKelas());
		brandTable.setItems(brandTableData);
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}
	
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void setPeriksa(Daftar daftar) {
		this.daftar = daftar;
		//namaPemohonField.setText(this.periksa.getPemohonParent().getNama());
		merekField.setText(this.daftar.getNamaMerekValue());
		kelasField.setText(this.daftar.getKelasValue());
	}
	
	@FXML
	private void handleAddFromDatabaseButton() {
		this.mainApp.showAddBrandTableForReportScreen(brandTableData);
	}
	
	@FXML
	private void handlCreateReportButton() {
		List<ReportBrand> reportBrands = new ArrayList<ReportBrand>();
		for(Brand brand : brandTableData) {
			ReportBrand reportBrand = new ReportBrand(brand.getCustomerParent().getNama(), brand.getMerekValue(),brand.getKelasValue());
			reportBrands.add(reportBrand);
		}
		JRBeanCollectionDataSource tableData = new JRBeanCollectionDataSource(reportBrands);
		try {
			new PrintReport().showReport(tableData, this.daftar, kesimpulanField.getText(), saranField.getValue());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
