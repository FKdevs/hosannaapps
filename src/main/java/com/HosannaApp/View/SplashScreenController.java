package com.HosannaApp.View;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.HosannaApp.MainApp;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SplashScreenController implements Initializable {
    @FXML
    private AnchorPane ap;
    @FXML
    private ImageView imageView;
    private MainApp mainApp;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    class ShowSplashScreen extends Thread {
        @Override
        public void run() {
            try {
                // change to 5000 for splash screen delay
                Thread.sleep(50);

                Platform.runLater(() -> {
                    Stage stage = new Stage();
                    try {
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("LoginPage.fxml"));
                        AnchorPane anchorPane = loader.load();

                        LoginPageController loginPageController = loader.getController();
                        loginPageController.setMainApp(mainApp);
                        stage.setTitle("HosanaApp");
                        mainApp.setPrimaryStage(stage);

                        Scene scene = new Scene(anchorPane);
                        stage.setScene(scene);
                        stage.show();
                        ap.getScene().getWindow().hide();

                    } catch (IOException ex) {
                        Logger.getLogger(SplashScreenController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            } catch (InterruptedException ex) {
                Logger.getLogger(SplashScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        InputStream imageFile = getClass().getClassLoader().getResourceAsStream("lamp.JPG");
        Image image = new Image(imageFile, 500, 500, false, false);
        imageView.setImage(image);
        imageView.setPreserveRatio(true);

        new ShowSplashScreen().start();

    }
}
