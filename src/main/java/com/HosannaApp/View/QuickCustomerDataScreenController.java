package com.HosannaApp.View;

import java.sql.SQLException;
import com.HosannaApp.DatabaseExecutor;
import com.HosannaApp.MainApp;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Util.AlertHelper;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class QuickCustomerDataScreenController {
	private MainApp mainApp;
	private Stage dialogStage;
	@FXML
	private TextField namaField;
	@FXML
	private TextArea alamatField;
	@FXML
	private TextField teleponField;
	@FXML
	private TextField emailField;

	@FXML
	private void initialize() {
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	@FXML
	private void handleConfirm() {
		if (inputCustomerCheck()) {
			Customer customer = new Customer(null, namaField.getText(), alamatField.getText(), "", "", "", "",
					teleponField.getText(), emailField.getText(), "");
			this.mainApp.addCustomerList(customer);

			try {
				DatabaseExecutor.addNewCustomerToDatabase(customer);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			this.clearInputFormAndRefreshList();
		}
		this.dialogStage.close();
	}

	@FXML
	private void handleClear() {
		this.clearInputFormAndRefreshList();
	}

	private void clearInputFormAndRefreshList() {
		namaField.clear();
		alamatField.clear();
		teleponField.clear();
		emailField.clear();
		this.mainApp.refreshCustomerAndKonsultanListInDatabaseMerek();
	}

	private boolean inputCustomerCheck() {
		if (namaField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Nama Field", "Nama Field Kosong");
			return false;
		}
		if (alamatField.getText().isEmpty()) {
			AlertHelper.showErrorWarning("Data Kurang", "Alamat Field", "Alamat Field Kosong");
			return false;
		}
		return true;
	}
}
