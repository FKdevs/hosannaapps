package com.HosannaApp.Util;

import java.util.Optional;

import com.HosannaApp.MainApp;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;

public class AlertHelper {

	private static Alert warningAlert = new Alert(AlertType.WARNING);
	private static Alert confirmationAlert = new Alert(AlertType.CONFIRMATION);

	private AlertHelper() {
	}
	
	public static void setMainApp(MainApp mainApp) {
		warningAlert.initOwner(mainApp.getPrimaryStage());
		confirmationAlert.initOwner(mainApp.getPrimaryStage());
	}

	public static void showErrorWarning(String title, String header, String content) {
		warningAlert.setTitle(title);
		warningAlert.setHeaderText(header);
		warningAlert.setContentText(content);
		warningAlert.showAndWait();
	}
	
	public static Optional<ButtonType> showConfirmationWarning(String title, String header, String content) {
		confirmationAlert.setTitle(title);
		confirmationAlert.setHeaderText(header);
		confirmationAlert.setContentText(content);
		return confirmationAlert.showAndWait();
	}
}
