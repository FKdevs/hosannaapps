package com.HosannaApp.Model;

import com.HosannaApp.DefaultObject;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.ImageView;

public class Paten {
	protected StringProperty merek;
	protected StringProperty kelas;
	protected StringProperty status;
	protected StringProperty deskripsiBarang;
	protected String nomor;
	protected Konsultan marketing;
	protected ImageView imageView;
	private static final String BLANK = "-";

	public Konsultan getKonsultan() {
		return this.marketing;
	}

	public void setKonsultan(Konsultan marketing) {
		this.marketing = marketing;
	}

	public StringProperty getMerek() {
		return this.merek;
	}

	public void setMerek(String merek) {
		this.merek.set(merek);
	}

	public StringProperty getKelas() {
		return this.kelas;
	}

	public void setKelas(String kelas) {
		if (kelas.isEmpty()) {
			this.kelas.set(BLANK);
		} else {
			this.kelas.set(kelas);
		}
	}

	public StringProperty getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		if (status.isEmpty()) {
			this.status.set(BLANK);
		} else {
			this.status.set(status);
		}
	}

	public String getMerekValue() {
		return this.merek.get();
	}

	public String getStatusValue() {
		return this.status.get();
	}

	public String getKelasValue() {
		return this.kelas.get();
	}

	public StringProperty getDeskripsiBarang() {
		return this.deskripsiBarang;
	}

	public String getDeskripsiBarangValue() {
		return this.deskripsiBarang.get();
	}

	public void setDeskripsiBarang(String deskripsiBarang) {
		if (deskripsiBarang.isEmpty()) {
			this.deskripsiBarang.set(BLANK);
		} else {
			this.deskripsiBarang.set(deskripsiBarang);
		}
	}

	public String getNomor() {
		return nomor;
	}

	public void setNomor(String nomor) {
		this.nomor = nomor;
	}

	public void setImage(ImageView imageView) {
		if (imageView.getImage() != null) {
			imageView.setFitWidth(30);
			imageView.setFitHeight(30);
			imageView.setPreserveRatio(true);
			imageView.setSmooth(true);
			this.imageView = imageView;
		} else {
			this.imageView = DefaultObject.getImageView();
		}
	}

	public ImageView getImageView() {
		return this.imageView;
	}
	
	public ObjectProperty<ImageView> getImageViewProperty() {
		return new SimpleObjectProperty<ImageView>(this.imageView);
	}
}
