package com.HosannaApp.Model;

public class ReportBrand {
	private String namaPemilik;
	private String merek;
	private String kelas;
	
	public ReportBrand(String namaPemilik, String merek, String kelas) {
		this.namaPemilik = namaPemilik;
		this.merek = merek;
		this.kelas = kelas;
	}
	public String getNamaPemilik() {
		return namaPemilik;
	}

	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}

	public String getMerek() {
		return merek;
	}

	public void setMerek(String merek) {
		this.merek = merek;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}
}
