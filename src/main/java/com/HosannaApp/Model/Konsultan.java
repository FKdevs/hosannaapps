package com.HosannaApp.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Konsultan {
	private ObservableList<Brand> brandList = FXCollections.observableArrayList();
	private StringProperty id = new SimpleStringProperty();
	private StringProperty nama = new SimpleStringProperty();
	private StringProperty alamat = new SimpleStringProperty();
	private StringProperty kewarganegaraan = new SimpleStringProperty();
	private static final String BLANK = "-";

	public Konsultan() {
	}

	public Konsultan(String id, String nama) {
		this.id = new SimpleStringProperty(id);
		this.nama = nama.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(nama);
		this.alamat = new SimpleStringProperty(BLANK);
	}

	public Konsultan(String nama) {
		this.id = new SimpleStringProperty("");
		this.nama = nama.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(nama);
		this.alamat = new SimpleStringProperty(BLANK);
	}

	public Konsultan(String id, String nama, String alamat, String kewarganegaraan) {
		this.id = new SimpleStringProperty(id);
		this.nama = nama.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(nama);
		this.alamat = alamat.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(alamat);
		if (!kewarganegaraan.isEmpty()) {
			this.kewarganegaraan = new SimpleStringProperty(kewarganegaraan);
		} else {
			this.kewarganegaraan = new SimpleStringProperty("ID (Indonesia)");
		}
	}

	public Konsultan clone() {
		Konsultan konsultanToClone = new Konsultan();
		konsultanToClone.setId(this.getIdValue());
		konsultanToClone.setNama(this.getNamaValue());
		konsultanToClone.setAlamat(this.getAlamatValue());
		return konsultanToClone;
	}

	public String getIdValue() {
		return this.id.get();
	}

	public StringProperty getIdProperty() {
		return this.id;
	}

	public void setId(String id) {
		this.id.set(id);
	}

	public String getNamaValue() {
		return this.nama.get();
	}

	public StringProperty getNamaProperty() {
		return this.nama;
	}

	public void setNama(String nama) {
		if (nama.isEmpty()) {
			this.nama.set(BLANK);
		} else {
			this.nama.set(nama);
		}
	}

	public String getAlamatValue() {
		return this.alamat.get();
	}

	public StringProperty getAlamatProperty() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		if (alamat.isEmpty()) {
			this.alamat.set(BLANK);
		} else {
			this.alamat.set(alamat);
		}
	}

	public String getKewarganegaraanValue() {
		return this.kewarganegaraan.get();
	}

	public StringProperty getKewarganegaraanProperty() {
		return this.kewarganegaraan;
	}

	public void setKewarganegaraan(String kewarganegaraan) {
		this.kewarganegaraan.set(kewarganegaraan);
		if (kewarganegaraan.isEmpty()) {
			this.kewarganegaraan.set("ID (Indonesia)");
		} else {
			this.kewarganegaraan.set(kewarganegaraan);
		}
	}

	public void addBrand(Brand brand) {
		brandList.add(brand);
	}

	public ObservableList<Brand> getBrand() {
		return this.brandList;
	}
}
