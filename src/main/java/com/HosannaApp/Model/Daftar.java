package com.HosannaApp.Model;

import java.time.LocalDate;

import com.HosannaApp.Util.DateUtil;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Daftar {

	Customer customer;
	Staff staff;
	private ObjectProperty<LocalDate> tanggalOrder;
	private ObjectProperty<LocalDate> tanggalLunas;
	private ObjectProperty<LocalDate> tanggalKirimHasil;
	private StringProperty idDaftar;
	private StringProperty nomorDO;
	private StringProperty jenisPermintaan;
	private StringProperty namaMerek;
	private StringProperty kelas;
	private StringProperty keteranganOrder;
	private StringProperty harga;
	private StringProperty invoice;
	private StringProperty caraBayar;
	private StringProperty saran;
	private StringProperty caraKirimHasil;
	private static final String BLANK = "-";
	private LocalDate defaultDate = LocalDate.of(1980, 1, 1);

	public Daftar() {
		this.idDaftar = new SimpleStringProperty("");
		this.nomorDO = new SimpleStringProperty("");
		this.customer = new Customer();
		this.staff = new Staff();
		this.jenisPermintaan = new SimpleStringProperty();
		this.namaMerek = new SimpleStringProperty();
		this.kelas = new SimpleStringProperty();
		this.keteranganOrder = new SimpleStringProperty();
		this.harga = new SimpleStringProperty();
		this.invoice = new SimpleStringProperty();
		this.caraBayar = new SimpleStringProperty();
		this.saran = new SimpleStringProperty();
		this.caraKirimHasil = new SimpleStringProperty();
		this.tanggalOrder = new SimpleObjectProperty<LocalDate>(LocalDate.now());
		this.tanggalLunas = new SimpleObjectProperty<LocalDate>();
		this.tanggalKirimHasil = new SimpleObjectProperty<LocalDate>();
	}

	public Daftar(String idOrder, String nomorDO, Customer customer, Staff staff, String jenisPermintaan,
			String namaMerek, String kelas, String keteranganOrder, String harga, String invoice, String caraBayar,
			String saran, String caraKirimHasil, LocalDate tanggalOrder, LocalDate tanggalLunas,
			LocalDate tanggalKirimHasil) {
		this.idDaftar = new SimpleStringProperty(idOrder);
		this.nomorDO = new SimpleStringProperty(nomorDO);
		this.customer = customer;
		this.staff = staff;
		this.namaMerek = new SimpleStringProperty(namaMerek);
		this.jenisPermintaan = kelas.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(jenisPermintaan);
		this.kelas = kelas.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(kelas);
		this.keteranganOrder = keteranganOrder.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(keteranganOrder);
		this.harga = harga.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(harga);
		this.invoice = invoice.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(invoice);
		this.caraBayar = caraKirimHasil.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(caraBayar);
		this.saran = caraKirimHasil.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(saran);
		this.caraKirimHasil = caraKirimHasil.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(caraKirimHasil);
		this.tanggalOrder = tanggalOrder == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalOrder);
		this.tanggalLunas = tanggalLunas == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalLunas);
		this.tanggalKirimHasil = tanggalKirimHasil == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalKirimHasil);
	}

	public Daftar(String idOrder, String nomorDO, Customer customer, Staff staff, String jenisPermintaan,
			String namaMerek, String kelas, String keteranganOrder, LocalDate tanggalOrder) {
		this.idDaftar = new SimpleStringProperty(idOrder);
		this.nomorDO = new SimpleStringProperty(nomorDO);
		this.customer = customer;
		this.staff = staff;
		this.jenisPermintaan = jenisPermintaan.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(jenisPermintaan);
		this.namaMerek = namaMerek.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(namaMerek);
		this.kelas = kelas.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(kelas);
		this.keteranganOrder = keteranganOrder.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(keteranganOrder);
		this.tanggalOrder = tanggalOrder == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalOrder);

		this.harga = new SimpleStringProperty(BLANK);
		this.invoice = new SimpleStringProperty(BLANK);
		this.caraBayar = new SimpleStringProperty(BLANK);
		this.saran = new SimpleStringProperty(BLANK);
		this.caraKirimHasil = new SimpleStringProperty(BLANK);
		this.tanggalLunas = new SimpleObjectProperty<LocalDate>(defaultDate);
		this.tanggalKirimHasil = new SimpleObjectProperty<LocalDate>(defaultDate);
	}

	public StringProperty getTanggalOrderProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalOrder.get()));
	}

	public void setTanggalOrder(LocalDate tanggalOrder) {
		if (tanggalOrder == null) {
			this.tanggalOrder = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalOrder = new SimpleObjectProperty<LocalDate>(tanggalOrder);
		}
	}

	public String getTanggalOrder() {
		return new SimpleStringProperty(DateUtil.format(tanggalOrder.get())).getValue();
	}

	public StringProperty getTanggalLunasProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalLunas.get()));
	}

	public void setTanggalLunas(LocalDate tanggalLunas) {
		if (tanggalLunas == null) {
			this.tanggalLunas = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalLunas = new SimpleObjectProperty<LocalDate>(tanggalLunas);
		}
	}

	public StringProperty getTanggalKirimHasilProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalOrder.get()));
	}

	public void setTanggalKirimHasil(LocalDate tanggalKirimHasil) {
		if (tanggalKirimHasil == null) {
			this.tanggalKirimHasil = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalKirimHasil = new SimpleObjectProperty<LocalDate>(tanggalKirimHasil);
		}
	}

	public LocalDate getTanggalOrderAsLocalDate() {
		return this.tanggalOrder.get();
	}

	public LocalDate getTanggalLunasAsLocalDate() {
		return this.tanggalLunas.get();
	}

	public LocalDate getTanggalKirimHasilAsLocalDate() {
		return this.tanggalKirimHasil.get();
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Staff getStaff() {
		return this.staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public StringProperty getIdDaftar() {
		return this.idDaftar;
	}

	public void setIdDaftar(String idDaftar) {
		this.idDaftar.set(idDaftar);
	}

	public String getIdDaftarValue() {
		return this.idDaftar.get();
	}

	public StringProperty getNomorDO() {
		return this.nomorDO;
	}

	public void setNomorDO(String nomorDO) {
		this.nomorDO.set(nomorDO);
	}

	public String getNomorDOValue() {
		return this.nomorDO.get();
	}

	public StringProperty getJenisPermintaan() {
		return this.jenisPermintaan;
	}

	public void setJenisPermintaan(String jenisPermintaan) {
		if (jenisPermintaan.isEmpty()) {
			this.jenisPermintaan.set(BLANK);
		} else {
			this.jenisPermintaan.set(jenisPermintaan);
		}
	}

	public String getJenisPermintaanValue() {
		return this.jenisPermintaan.get();
	}

	public StringProperty getNamaMerek() {
		return this.namaMerek;
	}

	public void setNamaMerek(String namaMerek) {
		if (namaMerek.isEmpty()) {
			this.namaMerek.set(BLANK);
		} else {
			this.namaMerek.set(namaMerek);
		}
	}

	public String getNamaMerekValue() {
		return this.namaMerek.get();
	}

	public StringProperty getKelas() {
		return this.kelas;
	}

	public void setKelas(String kelas) {
		if (kelas.isEmpty()) {
			this.kelas.set(BLANK);
		} else {
			this.kelas.set(kelas);
		}
	}

	public String getKelasValue() {
		return this.kelas.get();
	}

	public StringProperty getKeteranganOrder() {
		return this.keteranganOrder;
	}

	public void setKeteranganOrder(String keteranganOrder) {
		if (keteranganOrder.isEmpty()) {
			this.keteranganOrder.set(BLANK);
		} else {
			this.keteranganOrder.set(keteranganOrder);
		}
	}

	public String getKeteranganOrderValue() {
		return this.keteranganOrder.get();
	}

	public StringProperty getHarga() {
		return this.harga;
	}

	public void setHarga(String harga) {
		this.harga.set(harga);
	}

	public String getHargaValue() {
		return this.harga.get();
	}

	public StringProperty getInvoice() {
		return this.invoice;
	}

	public void setInvoice(String invoice) {
		if (invoice.isEmpty()) {
			this.invoice.set(BLANK);
		} else {
			this.invoice.set(invoice);
		}
	}

	public String getInvoiceValue() {
		return this.invoice.get();
	}

	public StringProperty getCaraBayar() {
		return this.caraBayar;
	}

	public void setCaraBayar(String caraBayar) {
		if (caraBayar.isEmpty()) {
			this.caraBayar.set(BLANK);
		} else {
			this.caraBayar.set(caraBayar);
		}
	}

	public String getCaraBayarValue() {
		return this.caraBayar.get();
	}

	public StringProperty getSaran() {
		return this.saran;
	}

	public void setSaran(String saran) {
		if (saran.isEmpty()) {
			this.saran.set(BLANK);
		} else {
			this.saran.set(saran);
		}
	}

	public String getSaranValue() {
		return this.saran.get();
	}

	public StringProperty getCaraKirimHasil() {
		return this.caraKirimHasil;
	}

	public void setCaraKirimHasil(String caraKirimHasil) {
		if (caraKirimHasil.isEmpty()) {
			this.caraKirimHasil.set(BLANK);
		} else {
			this.caraKirimHasil.set(caraKirimHasil);
		}
	}

	public String getCaraKirimHasilValue() {
		return this.caraKirimHasil.get();
	}
}
