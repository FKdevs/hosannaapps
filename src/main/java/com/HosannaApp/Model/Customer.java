package com.HosannaApp.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Customer {
	private ObservableList<Brand> brandList = FXCollections.observableArrayList();
	private ObservableList<Daftar> daftarList = FXCollections.observableArrayList();
	private String id;
	private String nama;
	private String alamat;
	private String provinsi;
	private String kota;
	private String kodePos;
	private String telepon;
	private String email;
	private String alamatKirim;
	private String kewarganegaraan;
	private static final String BLANK = "-";

	public Customer() {
	}

	public Customer(String idCustomer) {
		this.id = idCustomer;
		this.nama = BLANK;
		this.alamat = BLANK;
		this.provinsi = BLANK;
		this.kota = BLANK;
		this.kodePos = BLANK;
		this.telepon = BLANK;
		this.email = BLANK;
		this.alamatKirim = BLANK;
		this.kewarganegaraan = BLANK;
	}

	public Customer(String idCustomer, String nama, String alamat, String alamatKirim, String provinsi, String kota,
			String kodePos, String telepon, String email, String kewarganegaraan) {
		this.id = idCustomer;
		this.nama = nama.contains("//|") ? nama.split("//|")[0] : nama;
		this.alamat = alamat;
		this.provinsi = provinsi.isEmpty() ? BLANK : provinsi;
		this.kota = kota.isEmpty() ? BLANK : kota;
		this.kodePos = kodePos.isEmpty() ? BLANK : kodePos;
		this.telepon = telepon.isEmpty() ? BLANK : telepon;
		this.email = email.isEmpty() ? BLANK : email;
		this.alamatKirim = alamatKirim.isEmpty() ? BLANK : alamatKirim;
		this.kewarganegaraan = kewarganegaraan.isEmpty() ? BLANK : kewarganegaraan;
	}

	public Customer clone() {
		Customer pemilikToClone = new Customer();
		pemilikToClone.setId(this.getId());
		pemilikToClone.setNama(this.getNama());
		pemilikToClone.setAlamat(this.getAlamat());
		pemilikToClone.setAlamatKirim(this.getAlamatKirim());
		pemilikToClone.setProvinsi(this.getProvinsi());
		pemilikToClone.setKota(this.getKota());
		pemilikToClone.setKodePos(this.getKodePos());
		pemilikToClone.setTelepon(this.getTelepon());
		pemilikToClone.setEmail(this.getEmail());
		for (Brand brand : this.brandList) {
			Brand clonedBrand = brand.clone();
			clonedBrand.setCustomerParent(pemilikToClone);
			pemilikToClone.getBrand().add(clonedBrand);
		}

		return pemilikToClone;
	}

	public void addBrand(Brand brand) {
		brandList.add(brand);
	}

	public ObservableList<Brand> getBrand() {
		return this.brandList;
	}

	public void addDaftar(Daftar daftar) {
		daftarList.add(daftar);
	}

	public ObservableList<Daftar> getDaftar() {
		return this.daftarList;
	}

	public String getKewarganegaraan() {
		return this.kewarganegaraan;
	}

	public void setKewarganegaraan(String kewarganegaraan) {
		if (kewarganegaraan.isEmpty()) {
			this.kewarganegaraan = BLANK;
		} else {
			this.kewarganegaraan = kewarganegaraan;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public StringProperty getNamaProperty() {
		return new SimpleStringProperty(nama);
	}

	public StringProperty getAlamatProperty() {
		return new SimpleStringProperty(alamat);
	}

	public StringProperty getAlamatKirimProperty() {
		return new SimpleStringProperty(alamatKirim);
	}

	public StringProperty getProvinsiProperty() {
		return new SimpleStringProperty(provinsi);
	}

	public StringProperty getKotaProperty() {
		return new SimpleStringProperty(kota);
	}

	public StringProperty getKodePosProperty() {
		return new SimpleStringProperty(kodePos);
	}

	public StringProperty getTeleponProperty() {
		return new SimpleStringProperty(telepon);
	}

	public StringProperty getEmailProperty() {
		return new SimpleStringProperty(email);
	}

	public StringProperty getKewarganegaraanProperty() {
		return new SimpleStringProperty(kewarganegaraan);
	}

	public void setNama(String nama) {
		this.nama = nama.contains("//|") ? nama.split("//|")[0] : nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		if (provinsi.isEmpty()) {
			this.provinsi = BLANK;
		} else {
			this.provinsi = provinsi;
		}
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		if (kota.isEmpty()) {
			this.kota = BLANK;
		} else {
			this.kota = kota;
		}
	}

	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		if (kodePos.isEmpty()) {
			this.kodePos = BLANK;
		} else {
			this.kodePos = kodePos;
		}
	}

	public String getTelepon() {
		return telepon;
	}

	public void setTelepon(String telepon) {
		if (telepon.isEmpty()) {
			this.telepon = BLANK;
		} else {
			this.telepon = telepon;
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (email.isEmpty()) {
			this.email = BLANK;
		} else {
			this.email = email;
		}
	}

	public void setAlamatKirim(String alamatKirim) {
		if (alamatKirim.isEmpty()) {
			this.alamatKirim = BLANK;
		} else {
			this.alamatKirim = alamatKirim;
		}
	}

	public String getAlamatKirim() {
		return this.alamatKirim;
	}

	public StringProperty getNamaAlamatProperty() {
		return new SimpleStringProperty(nama + "|" + alamat);
	}
}
