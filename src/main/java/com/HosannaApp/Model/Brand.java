package com.HosannaApp.Model;

import java.time.LocalDate;

import com.HosannaApp.DefaultObject;
import com.HosannaApp.Util.DateUtil;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.ImageView;

public class Brand extends Paten {

	private Customer pemilikParent;
	private ObjectProperty<LocalDate> tanggalPendaftaran;
	private ObjectProperty<LocalDate> tanggalPengumuman;
	private ObjectProperty<LocalDate> tanggalKepemilikan;
	private ObjectProperty<LocalDate> tanggalKadarluarsa;
	private LocalDate defaultDate = LocalDate.of(1980, 1, 1);
	private static final String BLANK = "-";

	public Brand() {
		this.nomor = "";
		this.pemilikParent = new Customer();
		this.marketing = new Konsultan();
		this.merek = new SimpleStringProperty();
		this.kelas = new SimpleStringProperty();
		this.status = new SimpleStringProperty();
		this.tanggalPendaftaran = new SimpleObjectProperty<LocalDate>(LocalDate.now());
		this.tanggalPengumuman = new SimpleObjectProperty<LocalDate>(LocalDate.now());
		this.tanggalKepemilikan = new SimpleObjectProperty<LocalDate>(LocalDate.now());
		this.tanggalKadarluarsa = new SimpleObjectProperty<LocalDate>(LocalDate.now());
		this.deskripsiBarang = new SimpleStringProperty();
	}

	public Brand(String nomor, Customer pemilik, String merek, String kelas, String status,
			LocalDate tanggalPendaftaran, LocalDate tanggalPengumuman, LocalDate tanggalKepemilikan,
			LocalDate tanggalKadarluarsa, String deskripsiBarang, Konsultan marketing, ImageView imageMerek) {

		this.nomor = nomor;
		this.pemilikParent = pemilik;
		this.merek = new SimpleStringProperty(merek);
		this.kelas = kelas.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(kelas);
		this.status = status.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(status);
		this.tanggalPendaftaran = tanggalPendaftaran == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalPendaftaran);
		this.tanggalPengumuman = tanggalPengumuman == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalPengumuman);
		this.tanggalKepemilikan = tanggalKepemilikan == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalKepemilikan);
		this.tanggalKadarluarsa = tanggalKadarluarsa == null ? new SimpleObjectProperty<LocalDate>(defaultDate)
				: new SimpleObjectProperty<LocalDate>(tanggalKadarluarsa);
		this.deskripsiBarang = deskripsiBarang.isEmpty() ? new SimpleStringProperty(BLANK)
				: new SimpleStringProperty(deskripsiBarang);
		this.marketing = marketing;

		if (imageMerek.getImage() != null) {
			imageMerek.setFitWidth(30);
			imageMerek.setFitHeight(30);
			imageMerek.setPreserveRatio(true);
			imageMerek.setSmooth(true);
			this.imageView = imageMerek;
		} else {
			this.imageView = DefaultObject.getImageView();
		}

	}

	public Brand clone() {
		Brand brandToClone = new Brand();
		brandToClone.setNomor(this.getNomor());
		brandToClone.setTanggalPendaftaran(this.getTanggalPendaftaranAsLocalDate());
		brandToClone.setTanggalPengumuman(this.getTanggalPengumumanAsLocalDate());
		brandToClone.setTanggalKepemilikan(this.getTanggalKepemilikanAsLocalDate());
		brandToClone.setTanggalKadarluarsa(this.getTanggalKadarluarsaAsLocalDate());
		brandToClone.setMerek(this.getMerekValue());
		brandToClone.setKelas(this.getKelasValue());
		brandToClone.setStatus(this.getStatusValue());
		brandToClone.setDeskripsiBarang(this.getDeskripsiBarangValue());
		brandToClone.setKonsultan(this.getKonsultan().clone());
		return brandToClone;
	}

	public Customer getCustomerParent() {
		return this.pemilikParent;
	}

	public void setCustomerParent(Customer Pemilik) {
		this.pemilikParent = Pemilik;
	}

	public StringProperty getTanggalPendaftaranProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalPendaftaran.get()));
	}

	public void setTanggalPendaftaran(LocalDate tanggalPendaftaran) {
		if (tanggalPendaftaran == null) {
			this.tanggalPendaftaran = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalPendaftaran = new SimpleObjectProperty<LocalDate>(tanggalPendaftaran);
		}
	}

	public String getTanggalPendaftaran() {
		return new SimpleStringProperty(DateUtil.format(tanggalPendaftaran.get())).getValue();
	}

	public LocalDate getTanggalPendaftaranAsLocalDate() {
		return this.tanggalPendaftaran.get();
	}

	public StringProperty getTanggalPengumumanProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalPengumuman.get()));
	}

	public void setTanggalPengumuman(LocalDate tanggalPengumuman) {
		if (tanggalPengumuman == null) {
			this.tanggalPengumuman = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalPengumuman = new SimpleObjectProperty<LocalDate>(tanggalPengumuman);
		}
	}

	public String getTanggalPengumuman() {
		return new SimpleStringProperty(DateUtil.format(tanggalPengumuman.get())).getValue();
	}

	public LocalDate getTanggalPengumumanAsLocalDate() {
		return this.tanggalPengumuman.get();
	}

	public StringProperty getTanggalKepemilikanProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalKepemilikan.get()));
	}

	public void setTanggalKepemilikan(LocalDate tanggalKepemilikan) {
		if (tanggalKepemilikan == null) {
			this.tanggalKepemilikan = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalKepemilikan = new SimpleObjectProperty<LocalDate>(tanggalKepemilikan);
		}
	}

	public String getTanggalKepemilikan() {
		return new SimpleStringProperty(DateUtil.format(tanggalKepemilikan.get())).getValue();
	}

	public LocalDate getTanggalKepemilikanAsLocalDate() {
		return this.tanggalKepemilikan.get();
	}

	public StringProperty getTanggalKadarluarsaProperty() {
		return new SimpleStringProperty(DateUtil.format(tanggalKadarluarsa.get()));
	}

	public void setTanggalKadarluarsa(LocalDate tanggalKadarluarsa) {
		if (tanggalKadarluarsa == null) {
			this.tanggalKadarluarsa = new SimpleObjectProperty<LocalDate>(defaultDate);
		} else {
			this.tanggalKadarluarsa = new SimpleObjectProperty<LocalDate>(tanggalKadarluarsa);
		}
	}

	public String getTanggalKadarluarsa() {
		return new SimpleStringProperty(DateUtil.format(tanggalKadarluarsa.get())).getValue();
	}

	public LocalDate getTanggalKadarluarsaAsLocalDate() {
		return this.tanggalKadarluarsa.get();
	}
}
