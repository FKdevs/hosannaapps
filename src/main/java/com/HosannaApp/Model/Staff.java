package com.HosannaApp.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Staff {

	private ObservableList<Daftar> daftarList = FXCollections.observableArrayList();
	private ObservableList<Brand> brandList = FXCollections.observableArrayList();
	private StringProperty idStaff;
	private StringProperty nama;
	private StringProperty cabang;
	private static final String BLANK = "-";

	public Staff() {
	}

	public Staff(String idStaff, String nama, String cabang) {
		this.idStaff = new SimpleStringProperty(idStaff);
		this.nama = nama.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(nama);
		this.cabang = cabang.isEmpty() ? new SimpleStringProperty(BLANK) : new SimpleStringProperty(cabang);
	}

	public Staff(String idStaff) {
		this.idStaff = new SimpleStringProperty(idStaff);
		this.nama = new SimpleStringProperty(BLANK);
		this.cabang = new SimpleStringProperty(BLANK);
	}

	public String getIdValue() {
		return this.idStaff.get();
	}

	public StringProperty getIdProperty() {
		return this.idStaff;
	}

	public void setId(String idStaff) {
		this.idStaff.set(idStaff);
	}

	public String getNamaValue() {
		return this.nama.get();
	}

	public StringProperty getNamaProperty() {
		return this.nama;
	}

	public void setNama(String nama) {
		if (nama.isEmpty()) {
			this.nama.set(BLANK);
		} else {
			this.nama.set(nama);
		}
	}

	public String getCabangValue() {
		return this.cabang.get();
	}

	public StringProperty getCabangProperty() {
		return this.cabang;
	}

	public void setCabang(String cabang) {
		if (cabang.isEmpty()) {
			this.cabang.set(BLANK);
		} else {
			this.cabang.set(cabang);
		}
	}

	public void addDaftar(Daftar daftar) {
		daftarList.add(daftar);
	}

	public ObservableList<Daftar> getDaftar() {
		return this.daftarList;
	}
}
