package com.HosannaApp;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;

import com.HosannaApp.Model.Daftar;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class PrintReport extends JFrame {
	private static final long serialVersionUID = 1L;

	public void showReport(JRBeanCollectionDataSource tableData, Daftar daftar, String kesimpulan, String saran)
			throws JRException, ClassNotFoundException, SQLException {

		// First, compile jrxml file.
        InputStream reportFileSrc = getClass().getClassLoader().getResourceAsStream("Report_Template.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(reportFileSrc);
		// Fields for report
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		//parameters.put("NamaPemohonField", periksa.getPemohonParent().getNama());
		parameters.put("MerekField", daftar.getNamaMerekValue());
		parameters.put("KelasField", daftar.getKelasValue());
		parameters.put("KesimpulanField", kesimpulan);
		parameters.put("SaranField",
				"Merek " + daftar.getNamaMerekValue() + " Kelas " + daftar.getKelasValue() + " " + saran);
		parameters.put("ItemDataSource", tableData);

		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list.add(parameters);

		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(list);
		JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
		JasperViewer viewer = new JasperViewer(print);
        viewer.setVisible(true);
        this.setSize(2000, 1000);
	}

}
