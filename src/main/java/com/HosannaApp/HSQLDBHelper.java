package com.HosannaApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HSQLDBHelper {
	private static String url = "jdbc:hsqldb:file:database\\hosannadb";
    private static String user = "admin";
    //private static String password = "admin";
    private static String password = "";
    private static Connection con;

	public static ResultSet getDatabaseResultSet(String query) {
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver").newInstance();

			con = DriverManager.getConnection(url, user, password);

            Statement stt = con.createStatement();
            ResultSet res = stt.executeQuery(query);

            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
	}
	
	public static void queryToDatabase(String query) {
        try {
            Statement st = con.createStatement();
            st.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
