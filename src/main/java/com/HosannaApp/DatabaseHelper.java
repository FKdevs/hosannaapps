package com.HosannaApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseHelper {
    /*private static String url = "jdbc:mysql://192.168.1.12:3306/hosannadb?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static String user = "admin";
    private static String password = "admin";*/
    
    private static String url = "jdbc:mysql://localhost:3306/hosannadb?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static String user = "root";
    private static String psd = "";
    
    private static Connection con;

    public static ResultSet getDatabaseResultSet(String query) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, user, psd);

            Statement stt = con.createStatement();
            ResultSet res = stt.executeQuery(query);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void queryToDatabase(String query) {
        try {
            Statement st = con.createStatement();
            st.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static Connection getCon() {
    	return con;
    }
}
