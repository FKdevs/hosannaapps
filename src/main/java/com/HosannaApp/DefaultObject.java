package com.HosannaApp;


import java.io.InputStream;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class DefaultObject {
	
	private DefaultObject() {
	}
	
	public static ImageView getImageView() {
		InputStream imageFile = DefaultObject.class.getClassLoader().getResourceAsStream("default.png");
		Image image = new Image(imageFile);
		ImageView imageMerek = new ImageView(image);
		imageMerek.setFitWidth(30);
		imageMerek.setFitHeight(30);
		imageMerek.setPreserveRatio(true);
		imageMerek.setSmooth(true);
        return imageMerek;
	}
}
