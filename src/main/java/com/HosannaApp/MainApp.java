package com.HosannaApp;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Calendar;
import com.HosannaApp.Model.Brand;
import com.HosannaApp.Model.Konsultan;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.Staff;
import com.HosannaApp.Util.AlertHelper;
import com.HosannaApp.View.AddBrandTableForReportController;
import com.HosannaApp.View.DatabaseMerekDataScreenController;
import com.HosannaApp.View.MainScreenController;
import com.HosannaApp.View.QuickCustomerDataScreenController;
import com.HosannaApp.View.KonsultanDataScreenController;
import com.HosannaApp.View.CustomerDataScreenController;
import com.HosannaApp.View.DaftarAddDataController;
import com.HosannaApp.View.DaftarEditCashierDataController;
import com.HosannaApp.View.DaftarEditDataController;
import com.HosannaApp.View.DaftarEditDataProcessDataController;
import com.HosannaApp.View.DaftarScreenController;
import com.HosannaApp.View.ReportDialogController;
import com.HosannaApp.View.RootScreenController;
import com.HosannaApp.View.SplashScreenController;
import com.HosannaApp.View.StaffDataScreenController;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private ObservableList<Daftar> daftarList = FXCollections.observableArrayList();
	private ObservableList<Konsultan> konsultanList = FXCollections.observableArrayList();
	private ObservableList<Customer> customerList = FXCollections.observableArrayList();
	private ObservableList<Brand> brandList = FXCollections.observableArrayList();
	private ObservableList<Staff> staffList = FXCollections.observableArrayList();

	private StaffDataScreenController staffDataScreenController;
	private CustomerDataScreenController customerDataScreenController;
	private KonsultanDataScreenController konsultanDataScreenController;
	private DatabaseMerekDataScreenController databaseMerekDataScreenController;

	public MainApp() {
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("HosannaApp");
		initSplashLayout();
	}

	public static void main(String[] args) {
		launch(args);
	}

	public ObservableList<Daftar> getDaftarList() {
		return daftarList;
	}

	public ObservableList<Konsultan> getKonsultanList() {
		return konsultanList;
	}

	public ObservableList<Staff> getStaffList() {
		return staffList;
	}

	public ObservableList<Brand> getBrandList() {
		return brandList;
	}

	public ObservableList<Customer> getCustomerList() {
		return this.customerList;
	}

	public void initSplashLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/SplashScreen.fxml"));
			AnchorPane splashLayout = loader.load();

			SplashScreenController splashScreenController = loader.getController();
			splashScreenController.setMainApp(this);

			Scene scene = new Scene(splashLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
			AlertHelper.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/RootScreen.fxml"));
			rootLayout = loader.load();

			RootScreenController rootScreenController = loader.getController();
			rootScreenController.setMainApp(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showMainScreen() {
		try {
			this.getDataKonsultanFromDatabase();
			this.getDataCustomerFromDatabase();
			this.getDataBrandFromDatabase();
			this.getDataStaffFromDatabase();
			this.getDataDaftarFromDatabase();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		initRootLayout();
		goToMainScreen();
		Scene scene = new Scene(rootLayout);
		primaryStage.setScene(scene);
		primaryStage.setMaximized(true);
		primaryStage.show();
	}

	public void backToMainScreen() {
		goToMainScreen();
	}

	private void goToMainScreen() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/MainScreen.fxml"));
			AnchorPane page = loader.load();
			rootLayout.setCenter(page);

			MainScreenController mainScreenController = loader.getController();
			mainScreenController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showDaftarScreen() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/DaftarScreen.fxml"));
			AnchorPane page = loader.load();
			rootLayout.setCenter(page);

			DaftarScreenController daftarScreenController = loader.getController();
			daftarScreenController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showCustomerScreen() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/CustomerDataScreen.fxml"));
			AnchorPane page = loader.load();
			rootLayout.setCenter(page);

			customerDataScreenController = loader.getController();
			customerDataScreenController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showDatabaseMerekScreen() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/DatabaseMerekDataScreen.fxml"));
			AnchorPane page = loader.load();
			rootLayout.setCenter(page);

			databaseMerekDataScreenController = loader.getController();
			databaseMerekDataScreenController.setMainApp(this);
			databaseMerekDataScreenController.setMainStage(primaryStage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showKonsultanScreen() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/KonsultanDataScreen.fxml"));
			AnchorPane page = loader.load();
			rootLayout.setCenter(page);

			konsultanDataScreenController = loader.getController();
			konsultanDataScreenController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showStaffScreen() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/StaffDataScreen.fxml"));
			AnchorPane page = loader.load();
			rootLayout.setCenter(page);

			staffDataScreenController = loader.getController();
			staffDataScreenController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showKonsultanScreenAsDialog() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/KonsultanDataScreen.fxml"));
			AnchorPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Add Konsultan");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			konsultanDataScreenController = loader.getController();
			konsultanDataScreenController.setMainApp(this);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	private void getDataKonsultanFromDatabase() throws SQLException {
		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from konsultan");
		try {
			while (resultSet.next()) {
				Konsultan konsultanToAdd = new Konsultan(resultSet.getString("IdKonsultan"),
						resultSet.getString("Nama"), resultSet.getString("Alamat"),
						resultSet.getString("Kewarganegaraan"));
				konsultanList.add(konsultanToAdd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	private void getDataStaffFromDatabase() throws SQLException {
		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from staff");
		try {
			while (resultSet.next()) {
				Staff staffToAdd = new Staff(resultSet.getString("IdStaff"), resultSet.getString("Nama"),
						resultSet.getString("Cabang"));
				staffList.add(staffToAdd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	private void getDataCustomerFromDatabase() throws SQLException {
		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from customer");
		try {
			while (resultSet.next()) {
				Customer customerToAdd = new Customer(resultSet.getString("IdCustomer"), resultSet.getString("Nama"),
						resultSet.getString("Alamat"), resultSet.getString("AlamatKirim"),
						resultSet.getString("Provinsi"), resultSet.getString("Kota"), resultSet.getString("KodePos"),
						resultSet.getString("Telepon"), resultSet.getString("Email"),
						resultSet.getString("Kewarganegaraan"));
				customerList.add(customerToAdd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	private void getDataDaftarFromDatabase() throws SQLException {
		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from daftar");
		try {
			while (resultSet.next()) {
				Calendar calendarDaftar = DateSqlToDate(resultSet.getDate("TanggalOrder"));
				LocalDate tanggalDaftar = LocalDate.of(calendarDaftar.get(Calendar.YEAR),
						calendarDaftar.get(Calendar.MONTH) + 1, calendarDaftar.get(Calendar.DAY_OF_MONTH));
				Calendar calendarLunas = DateSqlToDate(resultSet.getDate("TanggalLunas"));
				LocalDate tanggalLunas = LocalDate.of(calendarLunas.get(Calendar.YEAR),
						calendarLunas.get(Calendar.MONTH) + 1, calendarLunas.get(Calendar.DAY_OF_MONTH));
				Calendar calendarKirimHasil = DateSqlToDate(resultSet.getDate("TanggalKirimHasil"));
				LocalDate tanggalKirimHasil = LocalDate.of(calendarKirimHasil.get(Calendar.YEAR),
						calendarKirimHasil.get(Calendar.MONTH) + 1, calendarKirimHasil.get(Calendar.DAY_OF_MONTH));

				Customer customerToAdd = getCustomerFromId(resultSet.getString("IdCustomer"));
				Staff staffToAdd = getStaffFromId(resultSet.getString("IdStaff"));
				Daftar daftar = new Daftar(resultSet.getString("IdDaftar"), resultSet.getString("NomorDO"),
						customerToAdd, staffToAdd, resultSet.getString("JenisPermintaan"),
						resultSet.getString("NamaMerek"), resultSet.getString("Kelas"),
						resultSet.getString("KeteranganOrder"), resultSet.getString("Harga"),
						resultSet.getString("Invoice"), resultSet.getString("CaraBayar"), resultSet.getString("Saran"),
						resultSet.getString("CaraKirimHasil"), tanggalDaftar, tanggalLunas, tanggalKirimHasil);

				customerToAdd.addDaftar(daftar);
				staffToAdd.addDaftar(daftar);
				daftarList.add(daftar);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	private void getDataBrandFromDatabase() throws SQLException {
		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from brand");

		try {
			while (resultSet.next()) {
				Calendar calendarPendaftaran = DateSqlToDate(resultSet.getDate("TanggalPendaftaran"));
				LocalDate tanggalPendaftaran = LocalDate.of(calendarPendaftaran.get(Calendar.YEAR),
						calendarPendaftaran.get(Calendar.MONTH) + 1, calendarPendaftaran.get(Calendar.DAY_OF_MONTH));

				Calendar calendarPengumuman = DateSqlToDate(resultSet.getDate("TanggalPengumuman"));
				LocalDate tanggalPengumuman = LocalDate.of(calendarPengumuman.get(Calendar.YEAR),
						calendarPengumuman.get(Calendar.MONTH) + 1, calendarPengumuman.get(Calendar.DAY_OF_MONTH));

				Calendar calendarKepemilikan = DateSqlToDate(resultSet.getDate("TanggalKepemilikan"));
				LocalDate tanggalKepemilikan = LocalDate.of(calendarKepemilikan.get(Calendar.YEAR),
						calendarKepemilikan.get(Calendar.MONTH) + 1, calendarKepemilikan.get(Calendar.DAY_OF_MONTH));

				Calendar calendarKedarluarsa = DateSqlToDate(resultSet.getDate("TanggalKadarluarsa"));
				LocalDate tanggalKadarluarsa = LocalDate.of(calendarKedarluarsa.get(Calendar.YEAR),
						calendarKedarluarsa.get(Calendar.MONTH) + 1, calendarKedarluarsa.get(Calendar.DAY_OF_MONTH));

				Customer customer = getCustomerFromId(resultSet.getString("IdCustomer"));
				Konsultan konsultan = getKonsultanFromId(resultSet.getString("IdKonsultan"));

				Blob blob = resultSet.getBlob("Image");
				ImageView imageView;
				InputStream imageStream = blob.getBinaryStream();  
				Image image = new Image(imageStream);
				imageView = new ImageView(image);
				
				Brand brand = new Brand(resultSet.getString("IdBrand"), customer, resultSet.getString("Merek"),
						resultSet.getString("Kelas"), resultSet.getString("Status"), tanggalPendaftaran,
						tanggalPengumuman, tanggalKepemilikan, tanggalKadarluarsa,
						resultSet.getString("DeskripsiBarang"), konsultan, imageView);

				customer.addBrand(brand);
				konsultan.addBrand(brand);
				brandList.add(brand);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	public void showAddDataDaftarScreen(String nomorDO) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/AddDataDaftarScreen.fxml"));
			BorderPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Data Daftar");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			DaftarAddDataController daftarEditAddDataController = loader.getController();
			daftarEditAddDataController.setMainApp(this);
			daftarEditAddDataController.setNomorDO(nomorDO);
			daftarEditAddDataController.setDialogStage(dialogStage);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showEditDataDaftarScreen(Daftar daftar, TableView<Daftar> mainDaftarTable) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/EditDataDaftarScreen.fxml"));
			BorderPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Data Daftar");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			DaftarEditDataController daftarEditDataController = loader.getController();
			daftarEditDataController.setMainApp(this);
			daftarEditDataController.setDaftarToEdit(daftar);
			daftarEditDataController.setMainDaftarTable(mainDaftarTable);
			daftarEditDataController.setDialogStage(dialogStage);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showEditCashierDataDaftarScreen(Daftar daftar) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/EditCashierDataDaftarScreen.fxml"));
			BorderPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Data Kasir");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			DaftarEditCashierDataController daftarEditCashierDataController = loader.getController();
			daftarEditCashierDataController.setMainApp(this);
			daftarEditCashierDataController.setDaftarToEdit(daftar);
			daftarEditCashierDataController.setDialogStage(dialogStage);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showEditDataProcessDataDaftarScreen(Daftar daftar, TableView<Daftar> mainDaftarTable) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/EditDataProcessDataDaftarScreen.fxml"));
			BorderPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Data Proses");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			DaftarEditDataProcessDataController daftarEditDataProcessDataController = loader.getController();
			daftarEditDataProcessDataController.setMainApp(this);
			daftarEditDataProcessDataController.setDaftarToEdit(daftar);
			daftarEditDataProcessDataController.setMainDaftarTable(mainDaftarTable);
			daftarEditDataProcessDataController.setDialogStage(dialogStage);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showQuickCustomerDataScreenAsDialog() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/QuickCustomerDataScreen.fxml"));
			BorderPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Quick Customer");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			QuickCustomerDataScreenController quickCustomerDataScreenController = loader.getController();
			quickCustomerDataScreenController.setMainApp(this);
			quickCustomerDataScreenController.setDialogStage(dialogStage);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showReportDialogScreen(Daftar daftar) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/ReportDialog.fxml"));
			ScrollPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Report");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			ReportDialogController reportDialogController = loader.getController();
			reportDialogController.setDialogStage(dialogStage);
			reportDialogController.setMainApp(this);
			// reportDialogController.setPeriksa(periksa);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showAddBrandTableForReportScreen(ObservableList<Brand> brandList) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("View/AddBrandTableForReport.fxml"));
			AnchorPane page = loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Report");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			AddBrandTableForReportController addBrandTableForReportController = loader.getController();
			addBrandTableForReportController.setDialogStage(dialogStage);
			addBrandTableForReportController.setMainApp(this);
			addBrandTableForReportController.setBrandListToAdd(brandList);
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addDaftarList(Daftar daftar) {
		this.daftarList.add(daftar);
	}

	public void addKonsultanList(Konsultan konsultan) {
		this.konsultanList.add(konsultan);
	}

	public void addCustomerList(Customer customer) {
		this.customerList.add(customer);
	}

	public void addBrandList(Brand brand) {
		this.brandList.add(brand);
	}

	public void addStaffList(Staff staff) {
		this.staffList.add(staff);
	}

	public void refreshCustomerAndKonsultanListInDatabaseMerek() {
		if (databaseMerekDataScreenController != null) {
			databaseMerekDataScreenController.setMainApp(this);
		}
	}

	public Calendar DateSqlToDate(java.sql.Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return calendar;
	}

	public Customer getCustomerFromId(String customerId) {
		for (Customer customer : customerList) {
			if (customer.getId().equals(customerId)) {
				return customer;
			}
		}

		return new Customer("0");
	}

	public Staff getStaffFromId(String staffId) {
		for (Staff staff : staffList) {
			if (staff.getIdValue().equals(staffId)) {
				return staff;
			}
		}

		return new Staff("0");
	}

	public Konsultan getKonsultanFromId(String konsultanId) {
		for (Konsultan konsultan : konsultanList) {
			if (konsultan.getIdValue().equals(konsultanId)) {
				return konsultan;
			}
		}

		return new Konsultan("0", "-");
	}
}
