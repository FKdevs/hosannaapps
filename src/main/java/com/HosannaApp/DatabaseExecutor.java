package com.HosannaApp;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import com.HosannaApp.Model.Brand;
import com.HosannaApp.Model.Konsultan;
import com.HosannaApp.Model.Customer;
import com.HosannaApp.Model.Daftar;
import com.HosannaApp.Model.Staff;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;

public class DatabaseExecutor {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static void updateDaftarToDatabase(Daftar daftar) {
		LocalDate tanggalKirimHasil = daftar.getTanggalKirimHasilAsLocalDate();
		String parsedTanggalKirimHasil = parseTanggal(tanggalKirimHasil);
		LocalDate tanggalLunas = daftar.getTanggalLunasAsLocalDate();
		String parsedTanggalLunas = parseTanggal(tanggalLunas);
		LocalDate tanggalOrder = daftar.getTanggalOrderAsLocalDate();
		String parsedTanggalOrder = parseTanggal(tanggalOrder);

		String queryToEditInDatabase = "UPDATE daftar set NomorDO = '" + daftar.getNomorDOValue() + "', TanggalOrder ='"
				+ parsedTanggalOrder + "', IdCustomer = '" + daftar.getCustomer().getId() + "', JenisPermintaan = '"
				+ daftar.getJenisPermintaanValue() + "', NamaMerek = '" + daftar.getNamaMerekValue() + "', Kelas = '"
				+ daftar.getKelasValue() + "', KeteranganOrder = '" + daftar.getKeteranganOrderValue() + "', Harga = '"
				+ daftar.getHargaValue() + "', Invoice = '" + daftar.getInvoiceValue() + "', TanggalLunas = '"
				+ parsedTanggalLunas + "', CaraBayar = '" + daftar.getCaraBayarValue() + "', Saran = '"
				+ daftar.getSaranValue() + "', TanggalKirimHasil = '" + parsedTanggalKirimHasil
				+ "', CaraKirimHasil = '" + daftar.getCaraKirimHasilValue() + "', IdStaff = '"
				+ daftar.getStaff().getIdValue() + "' where IdDaftar = '" + daftar.getIdDaftarValue() + "'";
		DatabaseHelper.queryToDatabase(queryToEditInDatabase);
	}

	public static void updateBrandToDatabase(Brand brand) {
		LocalDate tanggalPendaftaran = brand.getTanggalPendaftaranAsLocalDate();
		String parsedTanggalPendaftaran = parseTanggal(tanggalPendaftaran);
		LocalDate tanggalPengumuman = brand.getTanggalPengumumanAsLocalDate();
		String parsedTanggalPengumuman = parseTanggal(tanggalPengumuman);
		LocalDate tanggalKepemilikan = brand.getTanggalKepemilikanAsLocalDate();
		String parsedTanggalKepemilikan = parseTanggal(tanggalKepemilikan);
		LocalDate tanggalKadarluarsa = brand.getTanggalKadarluarsaAsLocalDate();
		String parsedTanggalKadarluarsa = parseTanggal(tanggalKadarluarsa);

		String queryToEditInDatabase = "UPDATE brand set Merek = '" + brand.getMerekValue() + "', Kelas='"
				+ brand.getKelasValue() + "', TanggalPendaftaran = '" + parsedTanggalPendaftaran
				+ "', TanggalPengumuman = '" + parsedTanggalPengumuman + "', TanggalKepemilikan = '"
				+ parsedTanggalKepemilikan + "', TanggalKadarluarsa = '" + parsedTanggalKadarluarsa + "', Status = '"
				+ brand.getStatusValue() + "', IdKonsultan = '" + brand.getKonsultan().getIdValue()
				+ "', DeskripsiBarang = '" + brand.getDeskripsiBarangValue() + "', IdCustomer = '"
				+ brand.getCustomerParent().getId() + "', Image = ? where IdBrand = '" + brand.getNomor() + "'";

		PreparedStatement preparedStmt;
		try {
			preparedStmt = DatabaseHelper.getCon().prepareStatement(queryToEditInDatabase);
			preparedStmt.setBinaryStream(1, JavaFXImageToInputStream(brand.getImageView()));
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void updateCustomerToDatabase(Customer customer) {
		String queryToEditInDatabase = "UPDATE customer set Nama = '" + customer.getNama() + "', Alamat = '"
				+ customer.getAlamat() + "', AlamatKirim = '" + customer.getAlamatKirim() + "', Provinsi = '"
				+ customer.getProvinsi() + "', Kota = '" + customer.getKota() + "', KodePos = '" + customer.getKodePos()
				+ "', Telepon = '" + customer.getTelepon() + "', Email = '" + customer.getEmail()
				+ "', Kewarganegaraan = '" + customer.getKewarganegaraan() + "' where IdCustomer = '" + customer.getId()
				+ "' ";
		DatabaseHelper.queryToDatabase(queryToEditInDatabase);
	}

	public static void updateKonsultanToDatabase(Konsultan konsultan) {
		String queryToEditInDatabase = "UPDATE konsultan set Nama = '" + konsultan.getNamaValue() + "', Alamat = '"
				+ konsultan.getAlamatValue() + "', Kewarganegaraan = '" + konsultan.getKewarganegaraanValue()
				+ "' where IdKonsultan = '" + konsultan.getIdValue() + "'";
		DatabaseHelper.queryToDatabase(queryToEditInDatabase);
	}

	public static void updateStaffToDatabase(Staff staff) {
		String queryToEditInDatabase = "UPDATE staff set Nama = '" + staff.getNamaValue() + "', Cabang = '"
				+ staff.getCabangValue() + "' where IdStaff = '" + staff.getIdValue() + "'";
		DatabaseHelper.queryToDatabase(queryToEditInDatabase);
	}

	public static void addNewCustomerToDatabase(Customer customer) throws SQLException {
		String addCustomerDatabaseQuery = "INSERT INTO customer(Nama, Alamat, AlamatKirim, Provinsi, Kota, KodePos, Telepon, Email, Kewarganegaraan) VALUES('"
				+ customer.getNama() + "', '" + customer.getAlamat() + "','" + customer.getAlamatKirim() + "' ,'"
				+ customer.getProvinsi() + "','" + customer.getKota() + "', '" + customer.getKodePos() + "', '"
				+ customer.getTelepon() + "', '" + customer.getEmail() + "', '" + customer.getKewarganegaraan() + "')";
		DatabaseHelper.queryToDatabase(addCustomerDatabaseQuery);
		ResultSet resultSet = DatabaseHelper
				.getDatabaseResultSet("select * from customer where Nama = '" + customer.getNama() + "'");
		try {
			while (resultSet.next()) {
				customer.setId(resultSet.getString("IdCustomer"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	public static void addNewKonsultanToDatabase(Konsultan konsultan) throws SQLException {
		String addKonsultanDatabaseQuery = "INSERT INTO konsultan(Nama,Alamat,Kewarganegaraan) VALUES('"
				+ konsultan.getNamaValue() + "','" + konsultan.getAlamatValue() + "','"
				+ konsultan.getKewarganegaraanValue() + "')";
		DatabaseHelper.queryToDatabase(addKonsultanDatabaseQuery);
		ResultSet resultSet = DatabaseHelper
				.getDatabaseResultSet("select * from konsultan where Nama = '" + konsultan.getNamaValue() + "'");
		try {
			while (resultSet.next()) {
				konsultan.setId(resultSet.getString("IdKonsultan"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	public static void addNewStaffToDatabase(Staff staff) throws SQLException {
		String addStaffDatabaseQuery = "INSERT INTO staff(Nama,Cabang) VALUES('" + staff.getNamaValue() + "','"
				+ staff.getCabangValue() + "')";
		DatabaseHelper.queryToDatabase(addStaffDatabaseQuery);
		ResultSet resultSet = DatabaseHelper
				.getDatabaseResultSet("select * from staff where Nama = '" + staff.getNamaValue() + "'");
		try {
			while (resultSet.next()) {
				staff.setId(resultSet.getString("IdStaff"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	public static void addNewBrandToDatabase(Brand brand) throws SQLException {
		LocalDate tanggalPendaftaran = brand.getTanggalPendaftaranAsLocalDate();
		String parsedTanggalPendaftaran = parseTanggal(tanggalPendaftaran);
		LocalDate tanggalPengumuman = brand.getTanggalPengumumanAsLocalDate();
		String parsedTanggalPengumuman = parseTanggal(tanggalPengumuman);
		LocalDate tanggalKepemilikan = brand.getTanggalKepemilikanAsLocalDate();
		String parsedTanggalKepemilikan = parseTanggal(tanggalKepemilikan);
		LocalDate tanggalKadarluarsa = brand.getTanggalKadarluarsaAsLocalDate();
		String parsedTanggalKadarluarsa = parseTanggal(tanggalKadarluarsa);

		String addBrandDatabaseQuery = "INSERT INTO brand(TanggalPendaftaran,TanggalPengumuman, TanggalKepemilikan, TanggalKadarluarsa, IdCustomer, IdKonsultan, Merek, Kelas, Status, DeskripsiBarang, Image) VALUES('"
				+ parsedTanggalPendaftaran + "','" + parsedTanggalPengumuman + "','" + parsedTanggalKepemilikan + "','"
				+ parsedTanggalKadarluarsa + "','" + brand.getCustomerParent().getId() + "','"
				+ brand.getKonsultan().getIdValue() + "','" + brand.getMerekValue() + "','" + brand.getKelasValue()
				+ "','" + brand.getStatusValue() + "','" + brand.getDeskripsiBarangValue() + "', ?)";
		
		PreparedStatement preparedStmt;
		try {
			preparedStmt = DatabaseHelper.getCon().prepareStatement(addBrandDatabaseQuery);
			preparedStmt.setBinaryStream(1, JavaFXImageToInputStream(brand.getImageView()));
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from brand where IdCustomer = '"
				+ brand.getCustomerParent().getId() + "' and Merek = '" + brand.getMerekValue() + "'");
		try {
			while (resultSet.next()) {
				brand.setNomor(resultSet.getString("IdBrand"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	public static void addNewDaftarToDatabase(Daftar daftar) throws SQLException {
		LocalDate tanggalOrder = daftar.getTanggalOrderAsLocalDate();
		String parsedTanggalOrder = parseTanggal(tanggalOrder);
		LocalDate tanggalLunas = daftar.getTanggalLunasAsLocalDate();
		String parsedTanggalLunas = parseTanggal(tanggalLunas);
		LocalDate tanggalKirimHasil = daftar.getTanggalKirimHasilAsLocalDate();
		String parsedTanggalKirimHasil = parseTanggal(tanggalKirimHasil);
		String addDaftarDatabaseQuery = "INSERT INTO daftar(NomorDO,TanggalOrder,IdCustomer,JenisPermintaan,NamaMerek,Kelas,KeteranganOrder,Harga,Invoice,TanggalLunas,CaraBayar,Saran,TanggalKirimHasil,CaraKirimHasil,IdStaff) VALUES('"
				+ daftar.getNomorDOValue() + "','" + parsedTanggalOrder + "','" + daftar.getCustomer().getId() + "','"
				+ daftar.getJenisPermintaanValue() + "','" + daftar.getNamaMerekValue() + "','" + daftar.getKelasValue()
				+ "','" + daftar.getKeteranganOrderValue() + "','" + daftar.getHargaValue() + "','"
				+ daftar.getInvoiceValue() + "','" + parsedTanggalLunas + "','" + daftar.getCaraBayarValue() + "','"
				+ daftar.getSaranValue() + "','" + parsedTanggalKirimHasil + "','" + daftar.getCaraKirimHasilValue()
				+ "','" + daftar.getStaff().getIdValue() + "')";
		DatabaseHelper.queryToDatabase(addDaftarDatabaseQuery);

		ResultSet resultSet = DatabaseHelper.getDatabaseResultSet("select * from daftar where IdCustomer = '"
				+ daftar.getCustomer().getId() + "' and NamaMerek = '" + daftar.getNamaMerekValue() + "'");
		try {
			while (resultSet.next()) {
				daftar.setIdDaftar(resultSet.getString("IdDaftar"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			resultSet.close();
		}
	}

	public static void deleteCustomerInDatabase(Customer customer) {
		String queryForDeleteData = "DELETE from customer where IdCustomer = '" + customer.getId() + "'";
		DatabaseHelper.queryToDatabase(queryForDeleteData);
	}

	public static void deleteBrandInDatabase(Brand brand) {
		String queryForDeleteDataPeriksa = "DELETE from brand where IdBrand = '" + brand.getNomor() + "'";
		DatabaseHelper.queryToDatabase(queryForDeleteDataPeriksa);
	}

	public static void deleteKonsultanInDatabase(Konsultan konsultan) {
		String queryForDeleteData = "DELETE from konsultan where IdKonsultan = '" + konsultan.getIdValue() + "'";
		DatabaseHelper.queryToDatabase(queryForDeleteData);
	}

	public static void deleteStaffInDatabase(Staff staff) {
		String queryForDeleteData = "DELETE from staff where IdStaff = '" + staff.getIdValue() + "'";
		DatabaseHelper.queryToDatabase(queryForDeleteData);
	}

	public static boolean emailValidator(String email) {
		Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		return matcher.find();
	}

	private static String parseTanggal(LocalDate tanggal) {
		if (tanggal != null) {
			return tanggal.format(formatter);
		} else {
			return null;
		}
	}

	private static InputStream JavaFXImageToInputStream(ImageView imageView) {
		BufferedImage bImage = SwingFXUtils.fromFXImage(imageView.getImage(), null);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			ImageIO.write(bImage, "png", outputStream);
			byte[] res = outputStream.toByteArray();
			InputStream inputStream = new ByteArrayInputStream(res);
			return inputStream;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
